require 'rails_helper'
require 'support/controller_macros'

RSpec.describe Admin::V2::OrdersController, :type => :controller do
  include Devise::TestHelpers
  extend ControllerMacros # 管理员登录

  login_admin

  describe "GET index" do
    it "returns http success" do
      get :index
      expect(response).to be_success
    end
  end

  describe "GET show" do
    let(:order) { FactoryGirl.create(:order) }
    it "returns http success" do
      get :show, { id: order.id.to_s }
      expect(response).to be_success
    end
  end

end
