require 'rails_helper'

RSpec.describe Api::AreasController, :type => :controller do

  describe "GET index" do
    let(:user) { FactoryGirl.create(:patient) }

    it "returns all areas" do
      get :index
      expect(response).to be_success
    end


    it "returns error message when no valid token given" do
      user.ensure_private_token!
      get :index, {:token => user.private_token }

      _body = response.body
      json_body = JSON(_body)

      expect(json_body).to include("status" => 'ok')
    end
  end

end
