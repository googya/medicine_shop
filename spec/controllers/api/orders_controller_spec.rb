require 'rails_helper'

RSpec.describe Api::OrdersController, :type => :controller do


  describe "GET show" do
    let(:user) { FactoryGirl.create(:patient) }
    let(:order) { FactoryGirl.create(:order, patient: user) }

    it "returns api correctly" do
      user.ensure_private_token!
      get :show,  { id: order.id.to_s, token: user.private_token, format: :json }

      expect(response).to be_success
    end
  end

end
