# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :admin_user do
    username "leslie"
    email "leslie@msn.com"
    mobile '18221849989'
    password '123456'
    role 'super'
  end
end
