# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product do
    identifier { rand } # 识别码
    barcode '123' # 电子监管码， 图片
    name 'hello'
    description 'description' # 说明
    otc true
    channel
    medicine
    after(:create) do |prod, evaluator|
      create(:price, product: prod, price: 100)
      create(:price, product: prod, price: 100)
    end
  end

  factory :otc_product, parent: :product do
    otc false
    channel

    association :medicine, factory: :otc_medicine
    after(:create) do |prod, evaluator|
      create(:price, product: prod, price: 50)
      create(:price, product: prod, price: 50)
    end
  end
end
