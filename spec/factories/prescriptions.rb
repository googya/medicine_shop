# Read about factories at http://github.com/thoughtbot/factory_girl
require_relative '../../lib/service_object/get_price'

FactoryGirl.define do

  factory :pres_item1, class: PresItem do
    quantity 5

    association :product, factory: :otc_product
  end

  factory :pres_item2, class: PresItem do
    quantity 3
    product
  end

  factory :prescription do
    note 'hello'
    status "unapproved"
    patient
    # pres_items { [ FactoryGirl.create(:pres_item1), FactoryGirl.create(:pres_item2) ]  }
    after(:create) do |pres, evaluator|
      create(:pres_item1, prescription: pres) do |i|
        i.price = GetPrice.price_at_area( i.product, pres.price_area )
      end
      create(:pres_item2, prescription: pres) do |i|
        i.price = GetPrice.price_at_area( i.product, pres.price_area )
      end
    end

  end

  factory :prescription_with_out_pres_items, class: Prescription do
    note 'hello'
    status "unapproved"
    patient
  end

end
