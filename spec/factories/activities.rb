# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :activity do
    title "MyString"
    content "MyString"
    image "MyString"
    status 'approved'
    manner 'donate'
    link 'http://baidu.com'
  end
end
