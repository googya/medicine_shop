FactoryGirl.define do
  factory :line_item do
    price 20
    quantity 5
    product
    max 10
  end


  factory :line_item_with_otc_medicine, parent: :line_item do

    association :product, factory: :otc_product

    quantity 5
    price 20
  end
end