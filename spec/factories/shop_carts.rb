# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :shop_cart do
    patient
    after(:build) do |sc, evaluator|
      sc.shop_cart_items.build( product: FactoryGirl.create(:product), quantity: 10)
    end
  end



  factory :shop_cart_item do
    quantity 4
    product
  end
end
