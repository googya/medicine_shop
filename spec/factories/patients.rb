# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  sequence(:mobile, (1000...10000).cycle ) {|z|z = z.next; "138#{z}#{z}" }
  factory :patient  do
    username "MyString"
    password '12345678'
    mobile
  end
end
