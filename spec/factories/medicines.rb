# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :medicine do
    identifier 'hello'
    barcode '23w3rwe'
    name '脚气灵'
    common_name '脚气灵'
    e_name '脚气灵'
    pack '脚气灵'
    dosage_form '脚气灵'
    indication '脚气'
    prohibited_combination ''
    description '脚气灵'
    desc_img ''
    is_health_insurance false
    term_of_validity ''
    registered_id ''
    registration_period ''
    storage_condition ''
    cold_chain false
    basic_drugs false
    otc false
  end

  factory :otc_medicine, parent: :medicine do

    identifier 'hello'
    barcode '23w3rwe'
    name '武汉健民'
    common_name '武汉健民'
    e_name '武汉健民'
    pack '武汉健民'
    dosage_form '武汉健民'
    indication '武汉健民'
    prohibited_combination ''
    description '武汉健民'
    desc_img ''
    is_health_insurance false
    term_of_validity ''
    registered_id ''
    registration_period ''
    storage_condition ''
    cold_chain false
    basic_drugs false

    otc true
  end
end
