#encoding: utf-8
require 'rails_helper'
require_relative '../../lib/service_object/gen_order'

RSpec.describe GenOrder do
  let(:patient) { FactoryGirl.create(:patient) }
  let(:img) {  File.open(File.dirname(__FILE__) + "/img.jpg")  }
  let(:items) { [ { quantity: 5, product_id: FactoryGirl.create(:otc_product).id  },
                  { quantity: 6, product_id: FactoryGirl.create(:product).id  } ] }


  # describe '#tidy' do
  #   it ' can reduce items ' do
  #     pid = shop_cart.shop_cart_items.last.product.id.to_s
  #     cart =  TidyShopCart.new(shop_cart, pid).tidy
  #     expect(cart.shop_cart_items.size).to eq(0)
  #   end
  # end
  #
  # describe '#xxx' do
  #   it 'has xxx method' do
  #     expect(TidyShopCart.new('', '').xxx).to eq('xxx')
  #   end
  # end

  describe '#generate_with_prescription' do
    it ' has 2 items' do
      order = GenOrder.new(patient, img, items).generate
      expect(order.line_items.count).to eql(2)
    end

    it ' has prescription' do
      order = GenOrder.new(patient, img, items).generate
      expect(order.prescription).to be_valid
    end

  end

  describe '#generate_without_prescription' do
    it ' has 2 items' do
      order = GenOrder.new(patient, nil, items).generate
      expect(order.line_items.count).to eql(2)
    end

    it ' has prescription' do
      order = GenOrder.new(patient, nil, items).generate
      expect(order.prescription).to be_nil
    end

  end

end
