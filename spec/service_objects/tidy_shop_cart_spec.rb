require 'rails_helper'
require_relative '../../lib/service_object/tidy_shop_cart'

RSpec.describe TidyShopCart do
  let(:patient) { FactoryGirl.crete(:patient) }
  let(:shop_cart) {  FactoryGirl.create(:shop_cart) }

  describe '#tidy' do
    it ' can reduce items ' do
      pid = shop_cart.shop_cart_items.last.product.id.to_s
      cart =  TidyShopCart.new(shop_cart, pid).tidy
      expect(cart.shop_cart_items.size).to eq(0)
    end
  end

  describe '#xxx' do
    it 'has xxx method' do
      expect(TidyShopCart.new('', '').xxx).to eq('xxx')
    end
  end
end
