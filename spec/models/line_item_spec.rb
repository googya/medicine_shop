require 'rails_helper'

RSpec.describe LineItem, :type => :model do
  describe  "#create" do
    let(:lt) { LineItem.new(product: FactoryGirl.create(:product), quantity: -1) }
    it 'quantity must greater than 0' do
      expect(lt.save).to be_falsey
      lt.quantity = 9
      expect(lt.save).to be_truthy
    end

    it 'can calculate total' do
      lt.quantity = 10
      lt.save

      expect(lt.total).to eq(1000)
    end

    it 'price is products price' do
      lt.quantity = 10
      lt.save

      expect(lt.price).to eq(lt.product.price)
    end
  end
end