require 'rails_helper'

RSpec.describe Order, :type => :model do

  let(:items) { FactoryGirl.build_list(:line_item, 3) }
  let(:patient) { FactoryGirl.create(:patient) }
  let(:prescription) { FactoryGirl.create(:prescription, patient: patient, pres_items: items.map {|e| e.attributes.slice('quantity', 'product_id') } );  }
  let(:o) { Order.create(line_items: items, patient: patient, prescription: prescription) }


  describe '#recalculate' do
    it 'should have 3 items' do
      expect(o.line_items.size).to eq(3)
    end

    it 's should have total' do
      o.recalculate_sum
      expect( o.total ).to eq(1500.0)
    end
  end

  describe '#state' do
    it 's init state is unapproved ' do
      expect(o.state).to eq(:unapproved)
      o.approve
      expect(String(o.state)).to eq('in_progress')
    end

    it 'can not jump state ' do
      expect { o.confirm }.to raise_exception
    end
  end

  describe '#modify_line_items' do
    let(:prod_4) { FactoryGirl.create(:otc_product) }
    let(:prod_5) { FactoryGirl.create(:otc_product) }
    let(:prod_6) { FactoryGirl.create(:product) }

    before do
      @items = [ { 'product_id' =>  String(prod_4._id), quantity: 100 }.with_indifferent_access,
                { 'product_id' =>  String(prod_5._id), quantity: 6 }.with_indifferent_access,
                {'product_id' =>  String(prod_6._id), quantity: 6 }.with_indifferent_access]
    end

    it ' adds new products and remove old ones ' do
      o.modify_line_items(@items.first(2))
      o.reload # 注意要重新加载
      expect(o.line_items.size).to eq(2)
      expect(o.line_items.where(is_deleted: true).count).to eq(0)
    end

    it ' updates successful ' do
      item1 =   FactoryGirl.create(:line_item, product: prod_4)
      item2 =  FactoryGirl.create(:line_item, product: prod_5)
      o2 = Order.create(line_items: [item1, item2], patient: patient)
      o2.modify_line_items(@items.first(2))
      expect(o2.line_items.where(is_deleted: true).count).to eq(0)
    end

    it ' fails when quantity is greater than max ' do
      o.modify_line_items(@items.first(2))

      # TODO: 这个地方得检查, 检查的地方是在 Order 中
      expect(o.save).to be_truthy
    end

  end


  describe '#create' do
    it ' should create message ' do
      o.save!
      sleep 0.1 # 异步创建的， 如何改进？
      expect(o.messages.count).to eq(1)
    end
  end

end