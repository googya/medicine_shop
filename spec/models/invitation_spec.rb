require 'rails_helper'

RSpec.describe Invitation, :type => :model do

  # let(:sender) { FactoryGirl.create(:patient) }
  # let(:receiver) { FactoryGirl.create(:patient) }

  describe  "#create" do

    it ' will produce a reg coupon when creating a patient' do
      FactoryGirl.create(:patient)
      expect(Coupon.count).to eq(1)
    end

    it ' will get a common coupon when recognized' do
      u = FactoryGirl.create(:patient)
      u.identity_recon; u.save
      expect( u.coupons.count ).to eq(2)
    end

    it ' the sender will get 3 coupons when the receiver identity_recon' do
      sender = FactoryGirl.create(:patient, status: 'id_recognized')
      expect(sender.coupons.count).to eq(1)

      receiver = FactoryGirl.create(:patient)
      receiver.received_invitation = Invitation.create(sender: sender)
      receiver.identity_recon; receiver.save
      expect(sender.coupons.count).to eq(4)
    end

  end

end