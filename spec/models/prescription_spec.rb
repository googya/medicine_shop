require 'rails_helper'

RSpec.describe Prescription, :type => :model do
    describe  '#save' do
      let(:patient) { FactoryGirl.create(:patient) }
      let(:pres) { FactoryGirl.create(:prescription, patient: patient, status: 'pending') }
      let(:pres_with_out_item) { FactoryGirl.create(:prescription_with_out_pres_items, patient: patient, status: 'pending') }


      it ' have no order when unapproved' do
        expect(pres.order).to eq(nil)
      end

      it ' has order when approved ' do
        pres.status = 'approved'
        pres.save!
        expect(pres.order.total).to eq(800.0)
      end

      it ' raise a error when approved without items ' do
        pres_with_out_item.status = 'approved'

        expect {  pres_with_out_item.save! }.to raise_error(Mongoid::Errors::Validations)
      end

      it ' can recalculate_sum ' do
        pres.status = 'approved'
        pres.save!
        order = pres.order
        order.line_items << FactoryGirl.create(:line_item_with_otc_medicine) # 只能追加非处方药
        order.save

        expect(order.total).to eq(1300)
      end

      it 'its order\'s state is approved when prescription approve  ' do
        pres.send(:gen_or_update_order)
        pres.status = 'approved'
        pres.save
        expect(pres.order.state.to_s).to eq("in_progress")
      end

    end

    describe "#update" do
      let(:patient) { FactoryGirl.create(:patient) }
      let(:pres) { FactoryGirl.create(:prescription, patient: patient, status: 'pending') }
      let(:pres_with_out_item) { FactoryGirl.create(:prescription_with_out_pres_items, patient: patient, status: 'pending') }
      let(:product) { FactoryGirl.create(:product) }

      it ' can add new pres_item ' do
        pres.pres_items.create( { quantity: 9, product: product } )
        pres.save
        expect(pres.pres_items.count).to eq(3)
      end
    end

    describe "#change_status" do
      let(:patient) { FactoryGirl.create(:patient) }
      let(:pres) { FactoryGirl.create(:prescription, patient: patient, status: 'pending') }
      let(:pres_with_out_item) { FactoryGirl.create(:prescription_with_out_pres_items, patient: patient, status: 'pending') }
      let(:product) { FactoryGirl.create(:product) }

      it ' will be approved if approve ' do
        pres.approve;
        pres.save
        expect(pres.status).to eq(String("approved"))
      end

      it ' can not be from declined to  approved' do
        pres.decline;
        pres.save

        expect { pres.approve }.to raise_exception
      end
    end
end
