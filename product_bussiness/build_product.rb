module ProductBussiness
  class BuildProduct
    def initialize(attributes:, channel:)
      @attributes = attributes
    end

    def build
      Product.create(attributes.merge!(channel: channel))
    end

    attr_reader :attributes, :channel
  end
end
