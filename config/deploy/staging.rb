# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult[net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start).
#
# Global options
# --------------
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
#
# And/or per server (overrides global)
# ------------------------------------
# server 'example.com',
#   user: 'user_name',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'user_name', # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: 'please use keys'
#   }


set :stage, :staging

set :rails_env, "production"
set :application, "medicine_shop"
set :branch, "refactor"
set :user, "lankr"
set :deploy_to, "/srv/#{fetch(:application)}"

server '69.164.201.79', user: 'lankr', roles: %w{web app db},
       ssh_options: {
           user: 'lankr',
           #password: 'l@nkr@sh'
           forward_agent: false,
           auth_methods: %w(publickey password),
           verbose: :debug
       }
