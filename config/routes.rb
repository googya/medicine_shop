Rails.application.routes.draw do

  devise_for :admin_user, :controllers => { :sessions => "admin/admin_user/sessions" }

  root to: 'home#index'

  namespace 'api', defaults: { format: 'json' } do
    resources :prescriptions,  only: [ :index, :create ] do
      post :create_pres_with_order_items, on: :collection
      post :create_pres_with_order_items_for_android, on: :collection
    end

    resources :drug_stores, only: [] do
      get :search, on: :collection
    end

    resources :products, only: [ :index, :show ] do
      get 'common', on: :collection
      get 'price_list', on: :member
    end

    resources :phone_calls, only: [ :create ]

    resources :areas, only: [ :index ] do
      collection do
        get :get_cities
        get :active_cities
      end
    end
    resources :orders, only: [ :index, :show, :update, :create ] do
      post :confirm_order, on: :member
      post :cancel_order, on: :member
      post :verify_coupon, on: :collection
    end
    resources :users, only: [] do
      post :sign_up, on: :collection
      post :login, on: :collection
      get :check_unread, on: :collection
      get :count_info, on: :collection
      put :update_password, on: :collection
      put :update_avatar, on: :collection
      get :my_prescription_doctors, on: :collection
      get :coupons, on: :member
      get :verify_invitation, on: :collection
      post :upload, on: :collection
    end

    resources :activities, only: [ :index ]
    resource :shop_cart, only: [ :create, :show, :destroy ] do
      post :check, on: :collection
      post :inc_item, on: :collection
      post :dec_item, on: :collection
    end

    resources :addresses, only: [ :index, :create, :update ] do
      post :set_default, on: :member
      get :default, on: :collection
    end
    resources :banners, only: [ :index ]

    resources :verify_codes, only: [ :create ] do
      post :verify, on: :collection
    end

    post 'devices/register'

    resources :messages, only: [] do
      get :latest, on: :collection
    end

    namespace :doc do
      resources :users, only: [] do
        post :sign_up, on: :collection
        post :login, on: :collection
        post :upload, on: :collection
        get :check_unread, on: :collection
        get :count_info, on: :collection
        put :update_password, on: :collection
        put :update_avatar, on: :collection
        get :prescription_list, on: :collection
        get :patient_list, on: :collection
      end
    end

  end

  namespace :admin do
    namespace :v2 do
      resources :prescriptions do
        member do
          get :get_doctors
        end
      end
      resources :products do
        get :filter_by, on: :collection
        get :export_to_csv, on: :collection
      end
      resources :medicines
      resources :hospitals
      resources :drug_stores
      resources :doctors do
        patch 'update_identity', on: :member
      end
      resources :users do
        patch 'update_identity', on: :member
      end
      resources :orders, only: [ :index, :show, :update, :destroy ] do
        patch :pay, on: :member
      end

      resources :departments
      resources :banners
      resources :dashboards, only: [ :index ]
      resources :prescription_doctors
      resources :activities

      resources :admin_users do
        get 'reset_password', on: :member
      end

      resources :coupons

      namespace :weixin do
        resources :products
        resources :activities
      end

      root to:  "prescriptions#index"
    end
  end

  namespace :mobiles do
    resources :prescriptions, only: [] do
      collection do
        get :new_prescription
        post :create_prescription
      end
    end

    resources :activities

    resources :users, only: [:new, :create] do
      collection do
        get :my_addresses
        get :my_center
        get :my_orders
        get :my_prescriptions
        get :my_coupons
        get :relate_user
        get :valid
        get :valid_success
        post :upload
        get :share_my_invitation_code
      end
      resources :addresses do
        member do
          get :set_default
        end
      end
      resources :coupons, only: [:show]
    end

    resources :orders, only: [:edit, :update, :show] do
      member do
        get :canncel
        get :edit_shipment
        put :update_shipment
      end
    end

  end

  resource :wechat, only:[:show, :create]
  resources :banners do
    get :news, on: :collection
  end

  resources :apps, only: [:index]

  mount RailsAdmin::Engine => '/admin/v1', as: 'rails_admin'

  mount API::Base, at: '/'
  mount GrapeSwaggerRails::Engine, at: "/documentation"

  # match '*path', via: :all, to: 'home#error_404'#只能放在最下面
end
