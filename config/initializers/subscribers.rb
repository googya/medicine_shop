ActiveSupport::Notifications.subscribe('user.scan_doctor_qr_code') do |name, start, finish, id, payload|

  Logs::Scan.find_or_create_by(payload)
end

ActiveSupport::Notifications.subscribe('order.be_confirmed') do |name, start, finish, id, payload|
  Thread.new {
    Logs::Order.create( payload.merge!( action_name: 'confirm' ) )
  }
end

ActiveSupport::Notifications.subscribe('order.be_canceled') do |name, start, finish, id, payload|
  Thread.new {
    Logs::Order.create(payload.merge!( action_name: 'cancel' ) )
  }
end


ActiveSupport::Notifications.subscribe('order.be_paid') do |name, start, finish, id, payload|
  Thread.new {
    Logs::Order.create(payload.merge!( action_name: 'paid' ) )
  }
end

ActiveSupport::Notifications.subscribe('user.has_read_messages') do |name, start, finish, id, payload|
  Message.where(action: payload[:action], is_read: false, user_id: payload[:user_id]).update_all(is_read: true)
end


ActiveSupport::Notifications.subscribe('user.home_user_message_read') do |name, start, finish, id, payload|
  mr = payload[:accountable].user_message_read
  if mr.blank?
    mr = payload[:accountable].create_user_message_read
  end
  mr.set(read_at: Time.now)
end