require Rails.root.join('lib', 'rails_admin_actions', 'rails_admin_approve_pres').to_s

RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  module RailsAdmin
    module Config
      module Actions
        class ApprovePres < RailsAdmin::Config::Actions::Base
          RailsAdmin::Config::Actions.register(self)
        end
      end
    end
  end

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    # approve_pres

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  #TODO 可以用 devise 来做认证
  config.authorize_with do
    authenticate_or_request_with_http_basic('Site Message') do |username, password|
      username == 'lankr' && password == 'lankr@sh'
    end
  end

  # config.authenticate_with do
  #   warden.authenticate! scope: :admin_user
  # end
  # config.current_user_method(&:current_admin_user)


  #隐藏模型
  # config.model 'Order' do
  #   visible false
  # end

  config.model 'Patient' do
    exclude_fields :password_digest, :private_token # 不显示字段

    object_label_method do # 代表该模型的属性或者方法
      :mobile
    end

  end

  config.model 'Medicine' do
    object_label_method do
      :common_name
    end

    list do
      field :common_name
      field :unit
      field :guiding_price
      field :storage_condition
      field :description
      field :dosage_form
      field :updated_at
    end
  end

  config.model 'Prescription' do
    list do
      exclude_fields :image
    end

    edit do
      include_all_fields

      field :status, :enum do
        enum do
          Prescription::STATUS
        end
      end

      field :image do
        read_only true
      end
    end

    config.model 'Activity' do
      include_all_fields

      field :manner, :enum do
        enum do
          Activity::MANNERS
        end
      end

      field :status, :enum do
        enum do
          Activity::STATUS
        end
      end
    end

    config.model 'Order' do
      include_all_fields
      field :patient_id do
        read_only true
      end
    end
  end
end
