module OrderBussiness
  module UseCases
    class ConfirmOrder
      def initialize(order:, attributes:, :is_taken_by_customer)
        @order = order
        @attributes = attributes
        @is_taken_by_customer = is_taken_by_customer
      end

      def confirm
        @order.modify_line_items(attributes.dig(:line_items))
        @order.update_attributes(order_update_params)
        @order.confirm
        @order
      end

      def order_update_params
        order_attributes = attributes.dig(
                    :address_name,
                    :ship_time,
                    :shipment_method,
                    :payment_method
                  ).merge!(_from: attributes[:user_agent])
        if is_taken_by_customer
          order_attributes[:address_name] = attributes[:address_name]
        else
          order_attributes[:id] = attributes[:id]
        end
        order_attributes
      end

      attr_accessor :order, :attributes, :is_taken_by_customer
    end
  end
end
