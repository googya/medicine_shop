# encoding: utf-8
namespace :order do

  task sync: :environment do
    Zqs::ZqsOrder.where(sync_status: false).each do |order|
      order.sync_wechat
    end

    p "*" * 10
    p "还有 #{Zqs::ZqsOrder.where(sync_status: false)} 未同步"
    p "*" * 10
  end

end
