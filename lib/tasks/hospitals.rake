# encoding: utf-8
require 'roo'

namespace :hs do

  task import: :environment do
    first_line = true

    s = Roo::Excelx.new('doc/hospital.xlsx')
    s.default_sheet = s.sheets.first
    s.each do |row|
      first_line = false and next if first_line

      province   = Area.roots.where(name: row[0]).first
      next unless province
      city       = province.children.where(name: row[1]).first


      Hospital.find_or_initialize_by(
        name:         row[2],
        city_id:      city.id,
        province_id:  province.id
      ).save if city
    end
  end

  # task areas: :environment do
  #   file = open("http://plato-hospital.dev/api/medicine_infos/areas")
  #   res = JSON.parse(file.read)
  #   res && res["areas"].each do |ele|
  #     area = Area.find_or_initialize_by(standard_id: ele['id'], name: ele['name'], level: 1)
  #     area.save
  #     !ele['children'].empty? && ele['children'].each do |child|
  #       item = Area.find_or_initialize_by(standard_id: child['id'], name: child['name'], level: 2, parent_id: area.id)
  #       item.save
  #       !child['children'].empty? && child['children'].each do |child_1|
  #         item_1 = Area.find_or_initialize_by(standard_id: child_1['id'], name: child_1['name'], level: 3, parent_id: item.id)
  #         item_1.save
  #       end
  #     end
  #   end
  # end

  # task hospitals: :environment do
  #   file = open("http://plato-hospital.dev/api/medicine_infos?limit=200")
  #   res = JSON.parse(file.read)
  #   res && res["hospitals"].each do |ele|
  #     region = Area.where(standard_id: ele['region']).first
  #     province = Area.where(standard_id: ele['province']).first
  #     city = Area.where(standard_id: ele['city']).first
  #     if region && province && city
  #       Hospital.find_or_initialize_by(
  #         standard_id: ele['standard_id'],
  #         name: ele['name'],
  #         region_id: region.id,
  #         province_id: province.id,
  #         city_id: city.id,
  #         phone: ele['phone'],
  #         lat: ele['lat'],
  #         lng: ele['lng']
  #       ).save
  #     end
  #   end
  # end

  task import_departments: :environment do
    departments = "骨科,心血管内科,妇产科,泌尿外科,眼科,神经内科,妇科,消化内科,儿科,神经外科,肿瘤科,呼吸内科,内分泌科,普外科,口腔科,中医科,皮肤科,耳鼻喉科,外科,内科,肾内科,心胸外科,心内科,肛肠科,产科,急诊科,血液科,骨伤科,麻醉科,肝胆外科,针灸科,骨外科,普通外科,肝病科,血液内科,胸外科,耳鼻咽喉科,放射科,男科,呼吸科,康复科,检验科,皮肤性病科,不孕不育科,感染科,核医学科,新生儿科,烧伤科,肾病科,中西医结合科,脑外科,肿瘤外科,血管外科,风湿免疫科,病理科,肿瘤内科,康复医学科,疼痛科,精神科,整形外科,整形美容科,肛肠外科,消化科,心血管科,风湿科,烧伤整形科,胸心外科,泌尿科,超声科,胃肠外科,乳腺科,心血管外科,手外科,体检中心,糖尿病科,老年病科,放疗科,影像科,口腔颌面外科,脊柱外科,小儿外科,介入科,生殖医学科,五官科,肝胆科,药剂科,乳腺外科,肾病内科,推拿科,微创外科,肾脏科,儿童保健科,心外科,耳鼻咽喉头颈外科,传染科,老年科,医学影像科,肾脏内科,肿瘤生物治疗中心,ICU科"
    departments.split(',').each do |name|
      t = Department.find_or_initialize_by(name: name)
      t.save if t.new_record?
    end
  end

end
