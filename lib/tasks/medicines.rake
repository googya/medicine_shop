#encoding: utf-8
require 'roo'
namespace :medicine do

  desc 'import medicines'
  task import: :environment do
    file = open("http://69.164.201.79/system/zhongshanxilu.xlsx")
    path = File.join(Dir.tmpdir, "medicine.xlsx")
    FileUtils.mv file, File.join(path)
    FileUtils.chmod(0644, path)

    roo = Roo::Excelx.new(path)

    (2..roo.last_row).each do |index|
      row = roo.row(index)
      manufacturer = Manufacturer.find_or_create_by(name: row[7]) if row[7].present?
      medicine = Medicine.new
      medicine.common_name = row[2]
      medicine.name = row[3]
      medicine.specification = row[4]
      medicine.unit = row[5]
      medicine.dosage_form = row[6]
      medicine.guiding_price = row[8]
      medicine.storage_condition = row[11]
      medicine.manufacturer = manufacturer.presence
      medicine.save!
    end
  end


  desc 'import yiyao medicines'
  task import_std: :environment do
    file = open("http://69.164.201.79/system/yiyao.xlsx")
    path = File.join(Dir.tmpdir, "medicine.xlsx")
    FileUtils.mv file, File.join(path)
    FileUtils.chmod(0644, path)

    roo = Roo::Excelx.new(path)

    title = roo.row(3)
    h = Hash[ title.zip Array(0..title.size-1) ]
    t = []
    (4..roo.last_row).each do |index|
      row = roo.row(index)
      manufacturer = Manufacturer.find_or_create_by(name: row[7]) if row[7].present?
      medicine = Medicine.new
      medicine.serial_no = row[0]
      medicine.common_name = row[3]
      medicine.name = row[4]
      medicine.specification = row[5]
      medicine.unit = row[6]
      medicine.dosage_form = row[11]
      medicine.guiding_price = row[1]
      medicine.term_of_validity = row[16]
      medicine.storage_condition = row[17]
      medicine.barcode = row[29]
      medicine.manufacturer = manufacturer.presence
      medicine.save!
    end
  end
end
