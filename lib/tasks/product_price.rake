#encoding: utf-8

namespace :product_price do

  desc 'import product_price'
  task import: :environment do

    path = File.join(Dir.home, "prices_area.csv")

    raise "price file not found " unless File.exist?(path)
    channel = Channel.find_or_create_by(name: '益药')

    CSV.readlines(path).each do |line|

      next if String(line[0]) == '地区'
      medicine = Medicine.where(serial_no: line[1]).first
      next if medicine.blank?
      area = Area.provinces.where(name: /#{line[0]}/).first
      next if area.blank?
      price = line[2]
      next if price.blank?

      product = channel.products.find_or_create_by(medicine_id: medicine)

      # print  line[1], " ", line[0], " ", price, "\n"
      next if product.blank? || area.blank? || price.blank?

      print  product.id, " ", area.name, " ", price, "\n"
      Price.find_or_create_by(price: price, product: product, area: area)

    end
  end
end
