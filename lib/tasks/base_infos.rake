# encoding: utf-8
namespace :base_infos do

  task prescription_doctors: :environment do
    names = %w(李医生 晶晶 吴嘉明  向顺 吴嘉明 周芳芳 洪旭 郭里 池姑娘 吴可嘉 韩健 石雨璐 张继兰 王西 杨光远 马清亮 于景欣 沈忠 孙树毓 郭智东 刘德华 恩惠 刘青鹏 光头强 光华 荣荣 陈文玥 depp 女户 项目测试 管轶栋 李政 李露潮 张祝清 郭继 红明 张继兰 ray 李 刘青鹏 孙小娥 李小笼 evan 李荣 行翀 周荣同 李露潮 season 小吴嘉 陈琰俊 周荣事 大梁 张祝清 向顺 evan 李珊 吴嘉明 胡源 周荣荣 张继兰 Jerry 孙益红 李医生 王伟 王医生 周鑫莉 末末 杨蕾 文大侠 郑迪)

    names.each do |ele|
      PrescriptionDoctor.create(name: ele, hospital_id: Hospital.all.sample.id)
    end
  end


end

