#encoding: utf-8
require 'roo'
namespace :infos do

  task import_medicine_and_product: :environment do

    channel = Channel.find_or_create_by(name: '益药')

    excel = Roo::Excel.new('doc/medicine_and_product.xls')

    (excel.first_row.succ..excel.last_row).each do |i|
      row = (excel.first_column..excel.last_column).collect do |j|
        excel.cell(i,j)
      end

      medicine = Medicine.find_or_initialize_by(serial_no: row[1])

      if medicine.new_record?
        medicine.name           = row[4]
        medicine.common_name    = row[3]
        medicine.specification  = row[5]
        medicine.save
      end

      product = channel.products.find_or_create_by(medicine_id: medicine)
      area    = Area.provinces.where(name: row[0]).first

      Price.where(product: product, area: area).delete_all

      if product && area
        price = Price.find_or_initialize_by(product: product, area: area)
        price.price = row[2]
        price.save

        p '=' * 10 << 'ok' << '=' * 10
      else
        p row
      end
    end
  end

end
