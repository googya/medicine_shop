#encoding: utf-8
require 'csv'
namespace :area do

  desc 'import areas'
  task import: :environment do
    path = File.join(Rails.root, "doc", "areas.csv")
    raise "area file not found " unless File.exist?(path)

    CSV.readlines(path).each do |line|

      next if String(line[0]) == '序号'
      _parent = Area.find_or_initialize_by( name: line[1], level: Area::PROVINCE )

      child  = Area.find_or_initialize_by( name: line[2], level: Area::CITY )
      _parent.save

      child.parent = _parent
      child.save
    end
  end
end
