# encoding: utf-8
require_relative '../weixin/weixin'
namespace :wechat do

  task :create_menu => :environment do
   post_url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=#{Weixin.access_token}"
    menu =  {
              "button": [

                {
                  "name": "拍照购药",
                  "type": "view",
                  "url": Weixin.generate_oauth_path("#{APP_CONFIG['host']}/mobiles/prescriptions/new_prescription")

                },
                {
                  "type": "view",
                  "name": "健康频道",
                  "url": Weixin.generate_oauth_path("#{APP_CONFIG['host']}/mobiles/activities")
                },
                {
                  "type": "view",
                  "name": "个人中心",
                  "url": Weixin.generate_oauth_path("#{APP_CONFIG['host']}/mobiles/users/my_center")
                }
              ]
            }

    res = HTTPClient.post(post_url, body: generate_post_hash(menu))

    p "*" * 100
    p res.body
    p "*" * 100
  end

  #处理菜单中文问题
  def generate_post_hash(post_hash)
    post_hash.to_json.gsub!(/\\u([0-9a-z]{4})/) { |s| [$1.to_i(16)].pack("U") }
  end

end
