module ServiceObject
  class ProductInOrders
    attr_accessor :user

    def initialize(_user)
      @user = _user
    end

    def product_quantity
      valid.pluck(:product_id, :quantity)
    end

    def common_product_ids( num = 5 )
      p_num_hash = Hash.new(0)

      product_quantity.each_with_object( p_num_hash ) do |e, hash|
        hash[e[0]] += Integer(e[1])
      end

      Array( p_num_hash ).lazy.sort do |a, b|
        b[1] <=> a[1]
      end.first( num ).map { |t| t[0] }

    end

    def valid
      lis = LineItem.where( :order_id.in => user.orders.pluck(:id) )
      product_ids = Product.where( :id.in => Array(lis.pluck(:product_id)) ).pluck(:id)
      lis.where(:product_id.in => Array(product_ids))
    end
  end
end