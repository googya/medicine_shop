require_relative '../../app/models/errors/errors'

class GetPrice
  attr_accessor :product, :app

  def initialize(product_id, app = nil)
    @product_id = product_id
    @app = app
  end

  def price
    # LESLIE: 需要根据渠道， 是否优惠， 何种优惠等生成价格
    t = Product.where(:_id => @product_id).first
    fail(Errors::ProductNotFound, 'ProductNotFound') if t.blank?
    t.price
  end

  def self.price_at_area(prod, area)
    area ||=  Area.where(name: '上海', level: 2).first
    unless prod.is_a?(Mongoid::Document)
      prod = Product.find(prod)
    end

    _price = prod.prices.where(area_id: area.id).first
    fail('Price not Found') if _price.blank?
    _price.price
  end
end
