# encoding: utf-8

require 'httpclient'
require_relative '../../app/models/errors/errors'
require_relative '../../lib/weixin/weixin'

# LESLIE: 这个方法得换个名字才行
class GenTicket
  attr_accessor :id
  def initialize(_id)
    @id = _id
  end


  def gen_ticket
    url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=#{Weixin.access_token}"
    infos = {
        expire_seconds: 1800,
        action_name: "QR_LIMIT_STR_SCENE",
        action_info: {
            scene: { scene_str:  @id }
        }
    }
    ret = HTTPClient.post(url, infos.to_json).body
    t = JSON(ret)
    raise("no support for this action") if t['errcode'].present?
    t['ticket']

  end

  def user_in_groups(user_id)
    url = "https://api.weixin.qq.com/cgi-bin/groups/getid?access_token=#{Weixin.access_token}"
    JSON HTTPClient.post(url, { openid: user_id }.to_json).body
  end
end
