require 'httpclient'
require 'active_support/core_ext/hash'

# API 文档 http://www.cosms.cn/home/static/sms_dl/1.html

module SmsSender

  URL = "http://api.cosms.cn/sms/putMt/"

  PARAMS = { msgFormat: 2, corpID: 2060691, loginName: 3052, password: 3052123,

             mtLevel: 1, subNumber: nil, linkID: nil, kindFlag: nil, MD5str: nil }.with_indifferent_access

  def send_sms(msg, mobs, suffix = "益药") # 消息内容， 以及接受的手机号
    return if Array(mobs).count == 0

    params = PARAMS
    params[:msg] = msg + "[ #{suffix} ]"
    params[:Mobs] = Array(mobs).join(',')
    params[:mtLevel] = mt_level(mobs)
    # binding.pry
    clt.get(URL, PARAMS)
  end

  def self.clt
    @clt ||= HTTPClient.new
  end

  def self.mt_level(mobs)
    _count = Array(mobs).count

    case
      when _count >= 100
        3
      when (10...100).include?(_count)
        2
      else
        1
    end
  end

  module_function :send_sms
end