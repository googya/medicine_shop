require 'jpush'

module JPushNoticeMessage

  def send_msg(ids, msg)
    client.sendPush(payload(ids, msg))
  end

  def self.client
    JPush::JPushClient.new(APP_CONFIG['app_key'], APP_CONFIG['master_secret'])
  end

  def self.payload(ids, msg)
    options = {
        platform: JPush::Platform.all,
        audience: JPush::Audience.build(registration_id: Array(ids)),
        options:  JPush::Options.build(apns_production: true),
        notification: JPush::Notification.new(alert: "#{msg}")
    }
    JPush::PushPayload.new(options)
  end

  module_function :send_msg
end