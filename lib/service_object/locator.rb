require 'net/http'
require 'json'
class Locator
  def self.drug_stores(longitude, latitude, page = 1)
    params = {:key => "53272d3cab868e1de0d5d3ac0ed970d6",
            :center => "#{longitude}, #{latitude}",
            :radius => 50000,
            :sortrule => "distance:1",
            :limit => 50,
            :page => page,
            :tableid => "54fff809e4b0328a66505b5b"}


    uri = URI('http://yuntuapi.amap.com/datasearch/around')
    uri.query = URI.encode_www_form(params)
    res = JSON (Net::HTTP.get_response(uri).body)
    res['status'] == 1 ? res['datas'] : []
  end

end
