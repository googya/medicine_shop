require_relative '../service_object/get_price'

class GenOrder
  attr_accessor :user, :items, :prescription_img

  def initialize(_user, _prescription_img = nil, _items)
    @user = _user
    @prescription_img = _prescription_img
    @items = _items
  end

  def generate
    if prescription_img.present?
      generate_with_prescription
    else
      generate_without_prescription
    end
  end

  private

  def generate_with_prescription # 主要要关联处方
    _order = user.orders.new(state: Order::UNAPPROVED)
    _prescription = user.prescriptions.new(image: prescription_img)
    _order.prescription = _prescription

    if items.present?
      _order.modify_line_items(Array(items))
      _prescription.create_items(Array(items))
    end

    _prescription.save
    _order.save
    _order
  end

  def generate_without_prescription
    return if items.blank?
    # 验证， 是否含处方药

    order = @user.orders.new
    items.each do |item|
      item = item.with_indifferent_access
      order_item = LineItem.new(quantity: item['quantity'],
                                product_id: item['product_id'], price: GetPrice.price_at_area( String(item['product_id']), order.price_area ))
      order.line_items << order_item
    end

    # order.modify_line_items(Array( items ))
    order.save

    order
  end


end