module SetVerifyCode

  def used( mobile, mode=VerifyCode::REG )
    vc = VerifyCode.find_or_create_by( mobile: mobile, mode: mode )
    vc.update_attribute( :status, VerifyCode::USED ) unless vc.status == VerifyCode::USED
    set_user(vc)
  end

  def set_user( verify_code, _type = 'patient' )
    u = User.where(mobile: verify_code.mobile).first
    if u.present?
      verify_code.user = u
      verify_code.use_for = _type
      verify_code.save!
    end
  end

  module_function :used, :set_user
end