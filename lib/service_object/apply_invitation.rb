#encoding: utf-8
require_relative '../../app/models/errors/errors'
require_relative '../../app/models/invitation'
require_relative '../../app/models/patient'
class ApplyInvitation
  attr_accessor :code, :receiver, :pa, :temp

  def initialize _code, _receiver = nil
    @code = _code
    @receiver = _receiver
  end


  def create_invitation

    raise(Errors::InvalidInvitationCode, "no such invitation code ") unless verify

    if temp.present?
      Invitation.create(other_sender_user: code, receiver: receiver)
    elsif pa.present?
      Invitation.create(sender: pa, receiver: receiver)
    else
      # do nothing
    end

  end

  def verify
    find_objects
    (pa.present? || temp.present?)
  end

  def find_objects
    @pa = Patient.where(invite_code: code).first
    @temp = Invitation.where(other_sender_user: code).first
  end

end