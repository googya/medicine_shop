# encoding: utf-8

require_relative '../service_object/get_price'

class GenOrderWithoutPrescription
  attr_accessor :user, :items

  def initialize(user, _items)
    @user = user
    @items = Array(_items)
  end

  def generate
    return if items.blank?
    # 验证， 是否含处方药

    order = @user.orders.new
    items.each do |item|
      order_item = LineItem.new(quantity: item['quantity'],
        product_id: item['product_id'], price: GetPrice.price_at_area( item['product_id'], order.price_area ))
      order.line_items << order_item
    end
    order.save

    order
  end
end