#encoding: utf-8
class Coupon::Validate
  attr_accessor :coupon, :total, :amount, :user

  def initialize(_coupon, _total, _user)
    @coupon = _coupon
    @total = _total
    @amount = _coupon.amount
    @user = _user
  end

  def validate
    valid_coupon? && can_use?
  end

  private
  def reg_coupon_ge1000
    (coupon.genus == Coupon::REG) && total >= 1000 && amount <= 100
  end

  def common_coupon_ge5000
    # use 100
    coupon.genus == Coupon::COMMON && total >= 5000 && amount <= 100
  end

  def common_coupon_ge2000
    # use 50
    coupon.genus == Coupon::COMMON && total >= 2000 && amount <= 50
  end

  def common_coupon_ge100
    # use 20
    coupon.genus == Coupon::COMMON && total >= 100 && amount <= 20
  end

  def valid_coupon?
    !coupon.expired? && !coupon.coupon_redemption.try(:redeemed?) && valid_patient?
  end

  def can_use?
    reg_coupon_ge1000 || common_coupon_ge5000 || common_coupon_ge2000 || common_coupon_ge100
  end

  def valid_patient?
    user.present? && String( coupon.coupon_redemption.try(:patient_id) ) == String( user.id )
  end

end