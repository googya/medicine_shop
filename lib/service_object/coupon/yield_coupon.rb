#encoding: utf-8

class Coupon::YieldCoupon

  attr_accessor :coupon
  def initialize _coupon_type, _amount = 20
    @coupon_type, @amount =  _coupon_type, _amount
  end

  def generate
    @coupon ||= Coupon.new( amount: @amount, genus: @coupon_type, valid_from: Time.now )
    @coupon.save!
    coupon
  end

  def assign_to(user)
    _redemption = CouponRedemption.new(coupon: generate)
    user.coupon_redemptions << _redemption
    user.save!
  end

  def self.add_coupon_to_user(coupon, user)
    user.coupon_redemptions << CouponRedemption.new(coupon: coupon)
    user.save!
  end
end