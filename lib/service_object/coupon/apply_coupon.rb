#encoding: utf-8
class Coupon::ApplyCoupon
  attr_accessor :coupon, :order, :total, :amount

  def initialize(_coupon, _order)
    @coupon = _coupon
    @order = _order
    @total = _order.line_items.pluck(:total).map(&:to_f).sum
    @amount = _coupon.amount
  end

  def apply

    return :invalid_or_can_not_use if ( !valid_coupon? || !can_use? )
    order.coupon = coupon
    _cd = coupon.coupon_redemption
    _cd.update_attribute(:redeemed_at, Time.now)
    order.save; _cd.save

    :coupon_used
  end

  private
  def reg_coupon_ge1000
    (coupon.genus == Coupon::REG) && total >= 1000 && amount <= 100
  end

  def common_coupon_ge5000
    # use 100
    coupon.genus == Coupon::COMMON && total >= 5000 && amount <= 100
  end

  def common_coupon_ge2000
    # use 50
    coupon.genus == Coupon::COMMON && total >= 2000 && amount <= 50
  end

  def common_coupon_ge100
    # use 20
    coupon.genus == Coupon::COMMON && total >= 100 && amount <= 20
  end

  def valid_coupon?
    !coupon.expired? && !coupon.coupon_redemption.try(:redeemed?) && valid_patient?
  end

  def can_use?
    reg_coupon_ge1000 || common_coupon_ge5000 || common_coupon_ge2000 || common_coupon_ge100
  end

  def valid_patient?
    order.patient_id.present? && String( coupon.coupon_redemption.try(:patient_id) ) == String( order.patient_id )
  end

end