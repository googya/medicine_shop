

class CheckMessage
  attr_reader :user
  def initialize(user)
    @user = user
  end

  def has_unread_new_orders?
    @user.messages.new_orders.unread.count > 0
  end

  def has_unread_new_activities?
    @user.messages.new_activities.unread.count > 0
  end

  def latest
    mr = user.user_message_read
    user.create_user_message_read if mr.nil?
    read_time = user.user_message_read.try(:read_at)

    if read_time.blank?
      _pres = user.prescription_events.last
      _order = user.order_events.last
    else
      _pres = user.prescription_events.where(:created_at.gt => read_time).last
      _order = user.order_events.where(:created_at.gt => read_time).last
    end
    max _pres, _order
  end

  private

  def max(e1, e2)
    if e1 && e2
      e1.try(:created_at) > e2.try(:created_at) ? e1 : e2
    elsif e1.blank?
      e2.presence
    else
      e1.presence
    end
  end

end