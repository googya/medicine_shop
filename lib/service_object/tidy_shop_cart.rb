class TidyShopCart
  def initialize(cart, product_ids)
    @cart = cart
    @product_ids = Array(product_ids)
  end

  # 购物车中会有很多物品
  # 提交的时候， 只会提交一部分
  # 将那些已经提交的，从购物车删除
  def tidy
    return if @cart.blank?
    tidy_specified(@product_ids)
    tidy_quantity_lte_zero
    @cart
  end

  def xxx
    'xxx'
  end

  # 清除指定产品 id 的
  def tidy_specified(product_ids)
    @cart.shop_cart_items.delete_all(:product_id.in => product_ids)
  end

  # 清除数量小于 0 的
  def tidy_quantity_lte_zero
    @cart.shop_cart_items.delete_all(:quantity.lte => 0)
  end
end
