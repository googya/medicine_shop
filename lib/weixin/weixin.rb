require 'digest/sha1'

module Weixin
  def access_token
    _path = WECHAT_CONFIG['access_token']
    return refresh_token  unless File.exist?(_path)

    _file = File.open(_path)
    return refresh_token if _file.mtime < (1.9).hours.ago

    token = _file.readlines.first
    return refresh_token if token.blank?

    token
  end

  def refresh_token
    raise("access_token file no setting") if WECHAT_CONFIG['access_token'].blank?
    appid = WECHAT_CONFIG['appid']
    secret = WECHAT_CONFIG['secret']
    raise('no wechat config') if appid.blank? || secret.blank?


    url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=#{appid}&secret=#{secret}"
    info = HTTPClient.get(url)
    _access_token = JSON(info.body)['access_token']
    raise("get access_token failure") if _access_token.blank?

    File.write(WECHAT_CONFIG['access_token'], _access_token)
    _access_token
  end

  def get_ticket
    _path = WECHAT_CONFIG['jsapi_ticket']
    return fetch_ticket  unless File.exist?(_path)

    _file = File.open(_path)
    return fetch_ticket if _file.mtime < (1.9).hours.ago

    token = _file.readlines.first
    return fetch_ticket if token.blank?
    token
  end


  def get_sign( jsapi_ticket, noncestr,  timestamp, url)
    str = "jsapi_ticket=#{jsapi_ticket}&noncestr=#{noncestr}&timestamp=#{timestamp}&url=#{url}"
    Digest::SHA1.hexdigest(str)
  end

  def fetch_ticket
    url = "http://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=#{ Weixin.access_token }&type=jsapi"
    info = JSON(HTTPClient.get(url).body)

    if info['errcode'].present? && info['errcode'] == 40001 # token 有问题， 重刷一次, 此处可优化
      refresh_token
      info = JSON(HTTPClient.get(url).body)
    end

    t = info.fetch('ticket') { '' }
    raise("fetch ticket failure") if t.blank?
    File.write(WECHAT_CONFIG['jsapi_ticket'], t)
    t
  end

  def get_pic(media_id)
    url = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token=#{ Weixin.access_token }&media_id=#{media_id}"
    info = HTTPClient.get(url).body

    if info['errcode'].present? && info['errcode'] == 40001 # token 有问题， 重刷一次, 此处可优化
      refresh_token
      info = HTTPClient.get(url).body
    end

    f = File.new("tmp/#{media_id}.jpg", 'w')
    f.syswrite(info)
    f.close
    f
  end

  def send_text(open_id, content)
    url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=#{ Weixin.access_token }"

    data =  {
              "touser": open_id,
              "msgtype":"text",
              "text":
              {
                "content": content
              }
            }.to_json

    info = HTTPClient.post(url, data).body

    if info['errcode'].present? && info['errcode'] == 40001 # token 有问题， 重刷一次, 此处可优化
      refresh_token
      info = HTTPClient.post(url, data).body
    end
  end

  def send_article(open_id, path_url)
    url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=#{ Weixin.access_token }"

    data = {
      "touser": open_id,
      "msgtype": "news",
      "news":{
        "articles": [
           {
              "title": "您的订单已生成。",
              "description": "请点击以确认",
              "url": path_url,
              "picurl": "http://sphapp.lankr.cn/mobile/index_approve.jpg"
            }
        ]
      }
    }.to_json
    info = HTTPClient.post(url, data.gsub(/\\u0026/, '&')).body

    if info['errcode'].present? && info['errcode'] == 40001 # token 有问题， 重刷一次, 此处可优化
      refresh_token
      info = HTTPClient.post(url, data.gsub(/\\u0026/, '&')).body
    end
  end

  #为按钮生成oauth链接
  def generate_oauth_path(origin_path)
    "https://open.weixin.qq.com/connect/oauth2/authorize?appid=#{WECHAT_CONFIG['appid']}&redirect_uri=#{origin_path}&response_type=code&scope=snsapi_base#wechat_redirect"
  end

  module_function :access_token, :refresh_token, :get_ticket, :get_sign, :fetch_ticket, :get_pic, :send_text, :send_article, :generate_oauth_path
end
