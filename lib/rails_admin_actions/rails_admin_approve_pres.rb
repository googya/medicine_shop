require 'rails_admin/config/actions'
require 'rails_admin/config/actions/base'

module RailsAdmin
  module Config
    module Actions
      class ApprovePres < RailsAdmin::Config::Actions::Base#处方评审
        register_instance_option :visible? do
          bindings[:object].class.name.downcase == 'prescription'
        end

        register_instance_option :member? do
          true
        end

        register_instance_option :http_methods do
          [:get, :put]
        end

        register_instance_option :controller do
          # Proc.new do
          #   @object.update_attribute(:status, Prescription::STATUS[1])
          #   flash[:notice] = "ok: #{@object.status}."
          #   redirect_to back_or_index
          # end


          proc do
            if request.get? # EDIT
              respond_to do |format|
                format.html { render @action.template_name }
                format.js   { render @action.template_name, layout: false }
              end

            elsif request.put? # UPDATE
              sanitize_params_for!(request.xhr? ? :modal : :update)

              @object.set_attributes(params[@abstract_model.param_key])

              @object.num = params[:num] if params[:num].present?
              @object.status = Prescription::STATUS[params[:status].to_i]


              if @object.save
                # @auditing_adapter && @auditing_adapter.update_object(@object, @abstract_model, _current_user, changes)
                respond_to do |format|
                  format.html { redirect_to_on_success }
                  format.js { render json: {id: @object.id.to_s, label: @model_config.with(object: @object).object_label} }
                end
              else
                handle_save_error :edit
              end
            end
          end

        end


        register_instance_option :link_icon do
          'icon-check'
        end
      end
    end
  end

end