class Activity
  include Mongoid::Document
  include Mongoid::Timestamps

  MANNERS = [ 'activity', 'donate' ]
  STATUS = [ 'unapproved', 'approved' ]

  field :title, type: String
  field :content, type: String
  field :link, type: String
  field :image, type: String
  field :manner, type: String
  field :end_at, type: DateTime # 活动结束时间
  field :status, type: String, default: 'unapproved'

  has_many :messages, as: :target
  belongs_to :channel

  scope :activity, -> { where(manner: 'activity') }
  scope :donate, -> { where( manner: 'donate' ) }

  validates_inclusion_of :manner, in: MANNERS
  validates_inclusion_of :status, in: STATUS

  # validates :link, format: {  with: /\A(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?\z/ }
  mount_uploader :image, ImageUploader

  scope :valid,  -> { all }

  after_create :generate_new_activity_for_every_user
  def generate_new_activity_for_every_user
    Thread.new do
      Patient.all.pluck(:id).each do |u_id|
        Message.create(user_id: u_id, target_id: self.id, action: Message::NEW_ACTIVITY)
      end
    end
  end

end
