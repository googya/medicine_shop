class Logs::Scan
  include Mongoid::Document
  include Mongoid::Timestamps

  field :action_name, type: String # 扫描关注 还是扫描购药等
  field :openid # 微信用户的 openid
  field :deleted_at # 标记删除

  belongs_to :doctor
  belongs_to :prescription_doctor

  default_scope -> { where(deleted_at: nil) }

  scope :scan, -> { where( action_name: 'scan' ) }
  scope :subscribe, -> { where( action_name: 'subscribe' )  }

end
