# encoding: utf-8

class Logs::ExchangeCode
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :exchange_code
  belongs_to :patient
end