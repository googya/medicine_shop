#encoding: utf-8

require_relative '../../../lib/service_object/sms_sender'


class Logs::Order
  include Mongoid::Document
  include Mongoid::Timestamps

  CONFIRM = 'confirm'
  CREATE = 'create'

  ACTION_NAMES = [ CONFIRM, CREATE ]

  field :action_name, type: String # [ 订单生成， 订单确认， 订单取消 , 订单完成 ]

  belongs_to :accountable, polymorphic: true
  belongs_to :order

  # validates :user, :order, presence: true

  after_create :notify_event
  def notify_event
    JPushNotify.perform_later self.class.to_s, self.id.to_s, message
  end

  after_create :send_sms
  def send_sms
    return if %w[development test].include?(Rails.env)
    if self.action_name == 'confirm' # 确认订单
      msg =  "  #{ accountable.username }订单确认了, 收货地址为：#{ self.order.try(:custom_ship_address) } "
      mobiles = AdminUser.pluck(:mobile)
      SmsJob.perform_later msg, mobiles
    end
  end

  def message
    case action_name
      when 'create'
        '您的订单生成了'
      when 'confirm'
        '您的订单确认了'
      when 'cancel'
        '您的订单被取消了'
      when 'paid'
        '您的订单完成了'
      else
        ''
    end
  end

  # 某种原因， 不能直接取到对应的 order, log.order => nil
  def order
    ::Order.find( read_attribute(:order_id) )
  end

end