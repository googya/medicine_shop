module Logs
  class Prescription # 审方日志
    include Mongoid::Document
    include Mongoid::Timestamps

    belongs_to :prescription, class_name: 'Prescription'
    field :status # 电子处方的状态, 同意或者拒绝
    belongs_to :accountable, polymorphic: true
    #

    after_create :queue_notification

    delegate :device_value, to: :accountable

    validates_presence_of :accountable

    def message
      if self.status == 'declined'
        "您的处方未通过审核， 理由是， #{ self.prescription.try(:declined_reason) }"
      elsif self.status == 'approved'
        '您的处方已经审核通过，请前往"我的订单"确认订单'
      else
        'something went wrong'
      end
    end


    private

    def queue_notification
      PrescriptionNoticeJob.perform_later self.id.to_s
    end

  end
end