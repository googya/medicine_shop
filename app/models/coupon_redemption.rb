#encoding: utf-8

# 目前一张优惠券只限一人使用
class CouponRedemption

  include Mongoid::Document

  belongs_to :coupon, index: true # counter_cache
  belongs_to :patient, index: true  #
  has_one :order

  field :redeemed_at, type: DateTime

  def redeemed?
    self.redeemed_at.present?
  end

  scope :valid, -> { where(redeemed_at: nil)  }

end