# encoding: utf-8

require './lib/service_object/get_price'
require './lib/service_object/sms_sender'
require './lib/weixin/weixin'
require_relative '../../app/models/mongoid/soft_delete'

class Prescription
  include Mongoid::Document
  include Mongoid::Timestamps
  include AASM
  include Mongoid::SoftDelete

  STATUS = [:pending, :approved, :declined]
  field :num # 处方号

  # field :medicines, type: Hash, default: {} #存药及其数量
  field :image # 每个处方都需要有张图片， 以便药剂师审核
  field :user_agent # 来源
  field :status, default: :pending
  field :note # 提交处方时候的备注
  field :declined_reason # 拒绝理由
  field :diagnosis # 诊断 （ 用户输入 ）
  field :pres_date, type: Date # 处方日期( 用户输入 )

  belongs_to :department, index: true
  belongs_to :hospital
  belongs_to :patient, index: true
  belongs_to :doctor, index: true
  belongs_to :pharmacist # 审方药师
  has_one :order
  embeds_many :pres_items
  has_many :events, class_name: 'Logs::Prescription'

  belongs_to :area, class_name: 'Area', index: true
  belongs_to :prescription_doctor, class_name: 'PrescriptionDoctor', index: true
  scope :pending, -> { where(status: :pending) }
  scope :approved, -> { where(status: :approved) }
  scope :declined, -> { where(status: :declined) }
  scope :in_zones, ->( area_ids ) { where(:area_id.in => area_ids ) }

  validates :patient, presence: true

  accepts_nested_attributes_for :pres_items,
    :reject_if => proc {|attributes|
      attributes['product_id'].blank? || attributes['quantity'].blank?
    },
    :allow_destroy => true


  delegate :hospital_address, :hospital_name, :name, to: :prescription_doctor, prefix: true, allow_nil: true
  delegate :name, to: :department, prefix: true, allow_nil: true

  # indexes
  index image: 1
  index note: 1
  index status: 1

  # author: depp
  mount_uploader :image, ImageUploader


  aasm column: :status do

    state :pending, initial: true

    state :approved
    state :declined

    event :approve do
      transitions from: :pending, to: :approved
    end

    event :decline do
      transitions from: :pending, to: :declined
    end

  end



  before_save do

    if self.changes['status'].present?
      _s_old, _s_new =  Array(changes['status']).map(&:to_s)
      if _s_old == 'pending'
        notifying_rejected if _s_new == 'declined'
        notifying_approved if _s_new == 'approved'
      end
    end
  end

  def items_info
    Array(pres_items).each_with_object([]) do |e, obj|
      obj << { product_id: e.product_id.to_s, price: e.price, quantity: e.quantity }
    end
  end

  def pres_drugs
    pres_items.select { |e| !e.otc? }.map(&:_id)
  end


  def create_items(items)
    Array(items).each do |item|
      item = item.with_indifferent_access
      price = GetPrice.price_at_area( item[:product_id], self.area ) # LESLIE: 需对商品、价格等确认
      pres_items << PresItem.new(item.slice( :product_id, :quantity )).tap { |e| e.price = price }
    end
  end

  # 当处方来自wechat时，审核之后需要推一条消息给相对应的open_id
  def send_to_wechat
    Weixin.send_article(patient.open_id, Weixin.generate_oauth_path("#{APP_CONFIG['host']}/mobiles/orders/#{order.id}/edit")) if user_agent == 'wechat'
  end

  # 价格所在省
  def price_area
    self.area.try(:parent)
  end

  private

  def notifying_rejected
    puts 'rejected'
    Thread.new do
      self.order.try(:delete) # 拒绝之后，删除无效订单
      self.events.create( status: self.status, accountable: patient )
    end
  end

  def notifying_approved
    puts 'approved'
    Thread.new do
      self.events.create( status: self.status, accountable: patient )
    end


    # ServiceObject::OrderGenerator.new(self).gen
    # raise(Errors::ItemInfoEmpty, 'item info is empty') if items_info.blank?
    gen_or_update_order
    approve_order
  end

  def gen_or_update_order
    hash = { items: [], total: 0, item_total: 0 }
    items_info.each_with_object(hash) do |e, obj|
      obj[:items] << LineItem.new(e)
      obj[:total] += e[:price].to_i * e[:quantity].to_i
      obj[:item_total] += e[:quantity].to_i
    end

    attrs = { patient_id: patient_id, total: hash[:total], item_total: hash[:item_total] }

    if self.order.present?
      _order = self.order
      _order.patient_id = patient_id
      _order.total = hash[:total]
      _order.item_total = hash[:item_total]
    else
      self.build_order(attrs)
    end

    # 给订单加来源,控制来自wechat的
    self.order._from = 'wechat' if self.user_agent == 'wechat'

    self.order.line_items = hash[:items]
    self.order.save!
  end

  validate :has_at_least_one_pres_item, if: -> { status == 'approved' }
  def has_at_least_one_pres_item
    if pres_items.blank?
      errors[:base] << 'at least one pres_item'
    end
  end

  after_create :sms_send
  def sms_send

    return if %w[development test].include?(Rails.env)
    Thread.new {
      mobiles = AdminUser.pluck(:mobile)
      SmsSender.send_sms( "  #{ patient.username }提交了处方, 请注意审核", mobiles )
    }

  end

  # 需求： 当处方所关联的订单确认之前， 可以修改处方的内容
  after_save :update_related_order, if: -> { order.present? && ![ Order::CONFIRM, Order::PAID ].include?(order.state)  }
  def update_related_order
    gen_or_update_order
  end

  def approve_order
    if self.order.state == Order::UNAPPROVED
      self.order.approve
      self.order.save
    end
  end


end
