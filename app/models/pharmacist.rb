# -*- coding: utf-8 -*-

class Pharmacist#药剂师
  include Mongoid::Document
  include Mongoid::Timestamps
  include Concerns::CommonUser


  field :practicing_certificate
  #has_many #审方记录

  #has_many :pharmacist_records
  
  mount_uploader :qr_code, ImageUploader
  mount_uploader :avatar, AvatarUploader

end