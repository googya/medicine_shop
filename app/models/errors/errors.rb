module Errors
  class LackImageError < StandardError; end
  class DoctorNotFound < StandardError; end
  class PatientNotFound < StandardError; end
  class DoctorNotFound < StandardError; end
  class ImageNotValid < StandardError; end
  class UserAlreadyExisted < StandardError; end
  class PasswordRequired < StandardError; end
  class TokenInvalid < StandardError; end
  class ItemInfoEmpty < StandardError; end
  class ProductNotFound < StandardError; end
  class NoWechatConfig < StandardError; end
  class PrescriptionNotApproved < StandardError; end
  class InvalidCode < StandardError; end
  class InvalidInvitationCode < StandardError; end
end