class PhoneCall
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :mobile, type: String


  belongs_to :user

end