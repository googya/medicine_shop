# encoding: utf-8

class Area
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name
  field :standard_id
  field :level

  validates_presence_of :name, :level

  PROVINCE = 2
  CITY = 3

  LEVELS = {
    1 => 'region',
    PROVINCE => 'province',
    CITY => 'city'
  }

  has_many   :children, class_name: 'Area', inverse_of: :parent
  belongs_to :parent,   class_name: 'Area', inverse_of: :children, index: true

  has_many :region_hospitals,   class_name: 'Hospital', inverse_of: :region
  has_many :province_hospitals, class_name: 'Hospital', inverse_of: :province
  has_many :city_hospitals,     class_name: 'Hospital', inverse_of: :city
  has_many :hospitals,          class_name: 'Hospital', inverse_of: :district
  has_many :prices, class_name: 'Price', inverse_of: :province


  scope :provinces, -> { where(level: PROVINCE)}
  scope :cities, -> { where(level: CITY) }

  scope :roots, -> { where(parent_id: nil)}

  index name: 1
  index level: 1

  def level_name
    LEVELS[level]
  end


  def to_hash
    {
        id:       self.id.to_s,
        name:     self.name,
        level:    self.level
    }
  end

end
