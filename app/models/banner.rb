# encoding: utf-8

class Banner
  include Mongoid::Document
  include Mongoid::Timestamps


  field :title, type: String
  field :poi, type: String
  field :position, type: String
  field :image, type: String
  field :begin_at, type: DateTime
  field :end_at, type: DateTime
  field :redirect_way, type: String # 跳转地址
  field :use, type: String # 用途 web or app

  index redirect_way: 1
  index poi: 1
  index title: 1

  mount_uploader :image, ImageUploader


  POI_OPTIONS = [
      ['左', 'left'],
      ['右', 'right'],
      ['上', 'up'],
      ['下', 'down']
  ]

  validates_presence_of :image, message: 'image must be existed'

  scope :valid, -> { all }
  # scope :live, -> {where("banners.begin_at <= ? and banners.end_at < ?", Time.now, Time.now)}

  def to_hash
    {
        id: id.to_s,
        title: title,
        poi: poi,
        position: position,
        url: image.try(:url),
        redirect_way: redirect_way
    }
  end

end