class Price

  include Mongoid::Document
  include Mongoid::Timestamps

  field :price, type: Float
  belongs_to :product, index: true
  belongs_to :area, class_name: 'Area', index: true

  # indexes
  index price: 1


  def to_hash

    {
      id: String(_id),
      product_id: String(product_id),
      area_id: String(area_id),
      price: read_attribute(:price)
    }

  end

end