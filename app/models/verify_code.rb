# encoding utf-8
require_relative 'errors/errors'
require_relative '../../lib/const_set_ext'

class VerifyCode
  include Mongoid::Document
  include Mongoid::Timestamps


  field :mobile
  field :code
  field :mode
  field :status
  field :use_for, type: String, default: 'patient'


  # my_const_set('ACTIONS', [
  #                           :NEW_ORDER, :NEW_ACTIVITY ])

  my_const_set("STATUSES", [ :USED, :IDLE ])
  # reg 注册， reset  重置
  my_const_set("MODES", [ :REG, :RESET ])

  validates :code, presence: true, uniqueness: true, format: { with: /\A\w{6}\z/ }
  validates :mobile, presence: true, format: { with:  /\A1[3|4|5|7|8][0-9]{9}\z/ } , uniqueness: { if: :cond }
  validates :status, inclusion: { in: VerifyCode::STATUSES }


  belongs_to :user, index: true

  # indexes
  index mobile: 1
  index code: 1
  index status: 1
  index use_for: 1
  index mobile: 1, mode: 1, use_for: 1

  scope :actives, ->{ where( status: VerifyCode::IDLE ) }

  def valid_code?
    self.status == VerifyCode::IDLE
  end

  def cond
    # Proc.new{ |code| code.mode == VerifyCode::REG }
    self.class.where(mobile: self.mobile, mode: self.mode, use_for: self.use_for).first.present?
  end



  def send_code

    # return if %w[development test].include?(Rails.env)

    raise Errors::InvalidCode, "#{self.code}已经被使用过" unless self.valid_code?

    msg = " 验证码为： #{ self.code } "

    SmsJob.perform_later msg, self.read_attribute(:mobile)

  end


  private

  after_initialize :set_defaults
  def set_defaults
    self.status ||= IDLE
    self.code ||= generate_code
  end

  def generate_code
    code = []
    1.upto(6) do
      code << (0..9).to_a.sample
    end
    code.join('')
  end

end