class AdminAbility
  include CanCan::Ability

  def initialize(user)
    if user.super_admin?
      can :manage, :all
    elsif user.content_admin?
      can :manage, [ Banner, Activity ]
    elsif user.audit_admin?
      can :manage, [ Order, Prescription, PrescriptionDoctor ]
    elsif user.logistics_admin?
      can :read, Order
    else

    end
  end
end
