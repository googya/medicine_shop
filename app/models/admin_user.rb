#encoding: utf-8
require_relative '../../lib/const_set_ext'

class AdminUser
  include Mongoid::Document
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  ## Database authenticatable
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time

  my_const_set('ROLES', [ :SUPER, :CONTENT, :AUDIT, :LOGISTICS, :SALE ])

  field :username, type: String
  field :role, type: String, default: 'normal'
  field :mobile
  has_and_belongs_to_many :zones, class_name: 'Area'

  # 后台人员有其对应之管理区域
  has_and_belongs_to_many :zones, class_name: 'Area'

  validates_inclusion_of :role, in: ROLES
  validates_uniqueness_of :username
  validates :mobile, presence: true
  validates :mobile, uniqueness: true

  ROLES.each do |role|
    define_method "#{role}_admin?" do
      read_attribute(:role) == role
    end
  end

end
