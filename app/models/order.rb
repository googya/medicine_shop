#coding: utf-8
require_relative '../../app/models/mongoid/soft_delete'
class Order
  include Mongoid::Document
  include Mongoid::Timestamps
  include AASM
  include Mongoid::SoftDelete

  # workflow_column :state

  UNAPPROVED    = :unapproved
  IN_PROGRESS = :in_progress # 进行中
  CONFIRM = :confirm # 已确认( 确认后不能修改 )
  PAID      = :paid   #已完成
  CANCELED  = :canceled #取消订单
  STATES = { in_progress: IN_PROGRESS, confirm: CONFIRM, paid: PAID, canceled: CANCELED, unapproved: UNAPPROVED }


  RECEIVER_INFO = Struct.new(:state, :city, :phone, :address, :name, :shipment_method, :payment_method, :ship_time)

  field :state, default: UNAPPROVED #订单状态

  field :item_total, type: BigDecimal,  default: 0.0#, null: false#商品的总个数
  field :email
  field :shipment_state
  field :payment_state
  field :shipment_method # 配送方式
  field :temp_shipment_address # 自定义的配送地址（以后如何统一？）
  field :ship_time, type: String
  field :payment_method # 支付方式

  # 订单编号
  field :_from, type: String # 来源(APP|WEB|WX)
  field :NO, type: String # 订单编号  SecureRandom.hex(8) 随机16位数字字母混掿

  belongs_to :ship_address, class_name: 'Address'

  has_many :line_items, autosave: true, dependent: :destroy # 级联删除
  belongs_to :drug_store

  belongs_to :patient, index: true
  belongs_to :prescription, index: true
  has_many :events, :class_name => 'Logs::Order'
  has_many :messages, as: :target
  belongs_to :coupon, index: true

  delegate :hospital, :doctor, :pres_drugs, to: :prescription, allow_nil: true # 医院 医生等信息从 prescription 处获取

  delegate :image_url, to: :prescription, allow_nil: true, prefix: true
  delegate :area, :price_area, to: :prescription, allow_nil: true

  attr_accessor :total, :sub_total, :deal_amount, :taxed_total, :deal_time

  validates :patient, presence: true # 用户一定存在

  scope :valid, -> {  where(:state.in => [ IN_PROGRESS, CONFIRM, PAID , UNAPPROVED ] ) }

  scope :recent,  -> { order_by(:created_at => :desc).limit(100) }

  # indexes
  index state: 1
  index ship_time: 1
  index payment_method: 1

  aasm column: :state do

    state :unapproved, initial: true
    state :in_progress
    state :confirm
    state :paid
    state :canceled

    event :approve do
      transitions from: UNAPPROVED, to: IN_PROGRESS
    end

    event :confirm do
      transitions from: :in_progress, to: CONFIRM
    end

    event :pay do
      transitions from: CONFIRM, to: PAID
    end

    event :cancel do
      transitions from: [ IN_PROGRESS, CONFIRM ], to: CANCELED
    end

  end

  # def to_builder
  #   jbuild do |json|
  #     json.status :ok
  #     json.msg :show
  #     json.doctor_name doctor.try(:name)
  #     json.hospital_name hospital.try(:name)
  #     json.state state
  #     json.line_items line_items do |item|
  #       json.id item.id.to_s
  #       json.(item, :price, :quantity)
  #       json.product_name item.product.try(:name)
  #       json.product_image item.product.try(:image_url)
  #       json.product_id item.product.id.to_s
  #     end
  #
  #     # 处方上的信息
  #     json.prescription_items prescription.pres_items do |pres_item|
  #       json.(pres_item, :price, :quantity)
  #       json.product_name pres_item.product.try(:name)
  #       json.product_image pres_item.product.try(:image_url)
  #       json.product_id pres_item.product.id.to_s
  #     end
  #
  #   end
  # end

  def custom_ship_address
    temp_shipment_address || ship_address.try(:state_city_address)
  end

  def jbuild(*args, &block)
    Jbuilder.new(*args, &block).attributes!
  end

  # TODO: 重新计算订单项的逻辑
  # 1 订单中， 处方药的数量不能超过处方中定义的
  # 2 订单中， 非处方药的数量不做限制
  # 3 客户端提交的数据是包含用户当前订单中的存在的数据（不含已删除的）


  def modify_line_items(new_items)
    # pres_drugs # 处方药, 数量有上限
    # 新增
    new_ids = new_items.map { |e| String( e['product_id'] ) }
    old_ids = line_items.map { |o| String( o.product_id ) }
    to_be_added = new_ids - old_ids
    to_be_deleted = old_ids - new_ids
    to_be_updated = old_ids & new_ids

    new_items.each do |item| # 新增药物只能是非处方药
      p_id = item['product_id']
      line_items << LineItem.new(item) if to_be_added.include?( p_id ) && Product.find(p_id).otc
    end

    to_be_deleted.each do |i|
      self.line_items.where(product_id: i).delete
    end

    to_updates = new_items.select do |i|
      to_be_updated.include?( i['product_id'] )
    end

    to_updates.each do |i|
      item = line_items.find_by( product_id: i['product_id'] )
      # item.update_attributes( i.slice( 'quantity', 'price' ).merge!( is_deleted: false ) )
      item.tap do |t|
        t.quantity = i['quantity'].to_i
        t.price = t.product.price
      end
      item.save!
    end
  end

  before_save :recalculate_sum

  def recalculate_sum
    if self.persisted?# && changes['line_items'].present?
      s = Hash.new(0)
      line_items.pluck(:quantity, :price).each_with_object(s) do |q_p_pair, hash|
        hash[:total] += q_p_pair[0].to_i * q_p_pair[1].to_i
        hash[:item_total] += q_p_pair[0].to_i
      end
      self.total, self.item_total = s.values_at(:total, :item_total)
    end
    self.NO = SecureRandom.hex(8)
  end

  def total
    line_items.pluck(:total).map(&:to_f).sum - coupon.try(:amount).to_f
  end

  def can_be_edited?
    String(state) == 'in_progress'
  end

  after_create :log_order_created
  def log_order_created
    Thread.new do
      self.events.create( accountable: self.patient, action_name: 'create' )
    end
  end

  after_create :create_new_order_message
  def create_new_order_message
    Thread.new do
      self.messages.create( user: self.patient, action: Message::NEW_ORDER )
    end
  end

  def identity_string
    _from.to_s + self.NO.to_s
  end

  def receiver_info
    json = RECEIVER_INFO.new
    if self.ship_address.present?
      json.state = self.ship_address.try(:state)
      json.city = self.ship_address.try(:city)
      json.phone = self.ship_address.try(:phone)
      json.address = self.ship_address.try(:address1)
      json.name = self.ship_address.try(:name)
    elsif self.temp_shipment_address.present?
      json.address = self.temp_shipment_address
    else
      json.state = patient.default_shipping_address.try(:state)
      json.city = patient.default_shipping_address.try(:city)
      json.phone = patient.default_shipping_address.try(:phone)
      json.address = patient.default_shipping_address.try(:address1)
      json.name = patient.default_shipping_address.try(:name)
    end

    json.shipment_method = self.shipment_method
    json.payment_method = self.payment_method
    json.ship_time = self.ship_time
    json
  end

  # def coupon
  #   coupon_redemption && coupon_redemption.coupon
  # end

  def coupon_amount
    coupon ? coupon.amount : 0.0
  end

  def item_prices
    line_items.collect{|item| item.total }
  end
end
