# encoding: utf-8

class DrugStore
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::SoftDelete


  field :province
  field :city
  field :district
  field :name
  field :store_type#类型
  field :phone#电话
  field :address
  field :rank#级别
  field :gps#图片
  field :image
  #editor

  has_many :inventories
  
  # author: depp
  has_many :addresses, as: :addressable

  mount_uploader :image, ImageUploader


end
