# encoding: utf-8

require_relative '../../lib/service_object/get_price'
class LineItem
  include Mongoid::Document
  include Mongoid::Timestamps

  # 订单项会被修改， 也会被删除

  belongs_to :product, index: true
  field :price, type: Float
  field :quantity, type: Integer, default: 1
  field :max, type: Integer, default: 1 #最大值为处方上的最大值， 此处防止处方修改， 导致数量不一致
  field :is_deleted, type: Boolean, default: false # 仅针对处方药
  field :total, type: Float

  validates :price, numericality:  { greater_than:  0.0 }, if: -> { read_attribute(:price).present? }
  validates :product, presence: true
  validates :quantity, numericality: { greater_than:  0 }

  # author: depp
  belongs_to :order, index: true #,  autosave: true

  delegate :id, :name, :image_url, :description, :otc, to: :product, allow_nil: true, prefix: true

  validates_presence_of :product # 确保商品一定存在
  validates_presence_of :quantity

  # indexes
  index max: 1
  index quantity: 1
  index price: 1


  before_save :pres_prod_quantity

  def pres_prod_quantity
    if !product_otc # 处方药

      # TODO: '没有处方项目的时候， 跳过'
      return unless order.try(:prescription).try(:pres_items)
      _pres_item = order.prescription.pres_items.find_by(product_id: product_id)
      self.max = _pres_item.quantity.to_i

      if quantity.to_i > self.max
        errors[:base] << ' quantity is greater than max '
        false
      end
    end
  end


  before_save :fix_price
  def fix_price
    self.price = GetPrice.price_at_area( self.product, self.order.try(:price_area) )
  end

  before_save :calculate_total
  def calculate_total
    # LESLIE: 目前简单的计算 item 的总价
    self.total = self.quantity.to_i * self.price.to_f
  end



  # before_save :ensure_total_is_correct
  # def ensure_total_is_correct
  # end

end
