class App
  include Mongoid::Document
  include Mongoid::Timestamps


  # has_many :contracts # 不能采用 through 来处理多对多关系
  #
  # field :name # app 的名称
  #
  # def endings
  #   Ending.in(id: contracts.pluck(:ending_id))
  # end


  field :name

  belongs_to :channel, index: true

  index( { name: 1 } )

end
