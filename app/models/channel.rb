class Channel
  include Mongoid::Document
  include Mongoid::Timestamps


  field :name, type: String


  has_many :products
  has_one :app
  has_many :activities

  index name: 1

end