# encoding: utf-8

class Address
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name
  field :address1
  field :address2
  field :city
  field :zip_code
  field :phone
  field :state_name
  field :alternative_phone
  field :company
  field :state # 省份 最好也能弄个模型
  field :country

  field :default, type: Boolean
  field :active, type: Boolean

  # author: depp
  # belongs_to :hospital
  # belongs_to :manufacturer
  # belongs_to :drug_store

  belongs_to :addressable, polymorphic: true, index: true

  index( { address1: 1, name: 1, phone: 1, city: 1, state: 1 } )

  def to_hash
    {
     id: _id.to_s,
     name: name,
     address: address1,
     zip_code: zip_code,
     phone: phone,
     state: state,
     city: city,
     default: default
    }
  end


  def state_city_address
    [state, city, address1].join(' ')
  end

  def to_ary
    [[name, phone, address1].join(' '), id.to_s]
  end

end
