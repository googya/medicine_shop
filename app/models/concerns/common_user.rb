module Concerns
  module CommonUser
    extend ActiveSupport::Concern
    
    included do

      include ActiveModel::SecurePassword

      field :username
      field :sin#Social Insurance Number
      field :id_num#身份证号
      field :mobile
      field :password_digest
      field :status
      field :registered_at
      field :role
      field :avatar #头像
      field :registered_from
      field :activation_code
      field :qr_code #二维码
      field :private_token#用户token, 用于 API 访问

      has_secure_password
      
      # mount_uploader :qr_code, ImageUploader
      # mount_uploader :avatar, AvatarUploader


      validates_presence_of :password, :on => :create

      def update_private_token
        random_key = "#{SecureRandom.hex(10)}:#{self.id}"
        self.update_attribute(:private_token, random_key)
      end

      def ensure_private_token!
        self.update_private_token if self.private_token.blank?
      end

    end
  end
end