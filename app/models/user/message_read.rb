class User::MessageRead

  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :patient
  field :read_at, type: DateTime

  index(read_at: 1)

end