require './lib/service_object/get_price'

class ShopCart
  include Mongoid::Document
  include Mongoid::Timestamps


  belongs_to :patient, index: true # 临时性质的， 保存用户的购物内容， 客户一旦提交订单即删除， 或者用户自己清空
  embeds_many :shop_cart_items
  belongs_to :area

  accepts_nested_attributes_for :shop_cart_items


  def self.new_with_items(items)
    new.tap do |c|
      Array(items).each do |item|

        item['price'] = GetPrice.price_at_area(item['product_id'], area)

        c.shop_cart_items.new(item)
      end
    end
  end


  def to_builder
    jbuild do |json|
      json.id String(id)
      json.patient_id patient_id.to_s
      json.items shop_cart_items.map(&:to_builder)
    end
  end

  def jbuild(*args, &block)
    Jbuilder.new(*args, &block).attributes!
  end

  def inc_item(product_id)
    price = GetPrice.price_at_area( product_id, self.area )
    item = self.shop_cart_items.find_or_initialize_by(product_id: product_id, price: price)
    item.quantity += 1
    item.save
    item
  end

  def dec_item(product_id)
    price = GetPrice.price_at_area( product_id, self.area )
    item = self.shop_cart_items.find_or_initialize_by(product_id: product_id, price: price)
    fail("quantity must greater or equal than 0") if item.quantity == 0
    item.quantity -= 1
    item.save
    item
  end
end
