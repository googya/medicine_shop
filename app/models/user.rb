require 'aasm'

class User
  include Mongoid::Document
  include Mongoid::Timestamps

  include AASM
  
  include ActiveModel::SecurePassword

  field :username
  field :sin#Social Insurance Number
  field :sin_image # 社保照片
  field :id_num # 身份证号
  field :hic_num # 医保卡号
  field :hic_image # 医保卡照片
  field :mobile
  field :password_digest
  field :status, default: 'id_unrecognized'
  field :registered_at
  field :role
  field :avatar # 头像
  field :registered_from
  field :activation_code
  field :qr_code # 二维码
  field :private_token # 用户token, 用于 API 访问
  field :detail_address #
  field :id_image # 身份证照片
  # field :password

  index role: 1
  index mobile: 1
  index status: 1
  index avatar: 1
  index private_token: 1
  index username: 1


  has_secure_password

  # mount_uploader :qr_code, ImageUploader
  mount_uploader :avatar, AvatarUploader

  mount_uploader :sin_image, ImageUploader
  
  mount_uploader :id_image, ImageUploader
  
  mount_uploader :hic_image, ImageUploader

  validates_presence_of :password, :on => :create
  
  # aasm
  aasm column: :status do
    state :id_unrecognized, :initial => true
    state :id_recognized
    
    event :identity_recon do 
      transitions :from => :id_unrecognized, :to => :id_recognized, :guard => :is_unrecognized?
    end
  
    event :identity_delay do
      transitions :from => :id_recognized, :to => :id_unrecognized, :guard => :is_recognized?
    end
  end
  
  def is_unrecognized?
    self.status == 'id_unrecognized'
  end
  
  def is_recognized?
    self.status == 'id_recognized'
  end

  def update_private_token
    random_key = "#{SecureRandom.hex(10)}:#{self.id}"
    self.update_attribute(:private_token, random_key)
  end

  def ensure_private_token!
    self.update_private_token if self.private_token.blank?
  end

  def avatar_url_with_host
    return ''  unless avatar_url.present?
    String(APP_CONFIG['host']) + String(avatar_url)
  end
end
