#encoding: utf-8
require_relative '../../lib/service_object/get_price'


class PresItem
  include Mongoid::Document
  include Mongoid::Timestamps

  field :price
  field :quantity
  belongs_to :product, index: true

  embedded_in :prescription

  delegate :otc?, to: :product

  index price: 1
  index quantity: 1

  before_save :set_total_price
  def set_total_price
    # self.price = self.quantity.to_i * GetPrice.new(self.product_id.to_s).price
    self.price = self.quantity.to_i * GetPrice.price_at_area(self.product, self.prescription.price_area)
  end
end
