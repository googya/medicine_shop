class Medicine
  include Mongoid::Document
  include Mongoid::Timestamps

  field :identifier # 识别码
  field :barcode # 电子监管码， 图片
  field :name # 商品名称
  field :common_name # 通用名称
  field :e_name # 英文名
  field :pack # 包装
  field :dosage_form # 剂型
  field :indication # 适应症
  field :prohibited_combination # 配伍禁忌
  field :description # 说明
  field :desc_img # 说明书图片
  field :image # 药品图片
  field :is_health_insurance, type: Boolean
  field :term_of_validity # 有效期
  field :registered_id # 注册证号
  field :registration_period # 注册有效期
  field :storage_condition # 存储条件
  field :cold_chain, type: Boolean # 是否冷链
  field :basic_drugs, type: Boolean # 是否基药
  field :otc, type: Boolean # 是否非处方药
  field :high_price, type: Float # 最高价
  field :guiding_price, type: Float # 指导价
  field :low_price, type: Float # 最低价
  field :specification, type: String # 规格
  field :unit # 计量单位
  field :serial_no # 药品编号， 来自其他数据库

  # field :price, type: Float # 此处可以使用 money-rails 插件

  belongs_to :manufacturer
  #editor
  # author: depp
  has_many  :products
  
  mount_uploader :image, ImageUploader


  delegate :name, to: :manufacturer, allow_nil: true, prefix: true
  scope :by_name, -> (search) { where(:name => /#{search}/) }

  # indexes
  index name: 1
  index serial_no: 1
  index unit: 1
  index specification: 1
  index otc: 1
  index image: 1
  index indication: 1
  index dosage_form: 1

end

