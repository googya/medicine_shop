# encoding: utf-8

class Product
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::SoftDelete

  attr_accessor :price

  # field :price, type: Float # 此处可以使用 money-rails 插件
  field :alias_name # 别名
  field :name # 商品名称

  field :description
  field :identifier, type: String
  field :barcode
  field :otc, type: Boolean

  has_many :inventories
  belongs_to :channel, index: true
  has_many :prices, class_name: 'Price'

  accepts_nested_attributes_for :prices, :reject_if => :all_blank, :allow_destroy => true

  # author: depp
  # has_many :medicines
  belongs_to :medicine, index: true #TODO 上面的一些属性， 直接从 medicine 处获得就好了

  delegate :manufacturer_name, to: :medicine, allow_nil: true
  delegate :indication, :specification, :unit, :dosage_form, :common_name,
           :identifier, :low_price, :high_price, :serial_no, to: :medicine,  allow_nil: true

  # TODO 增加药品和渠道的验证 ( must have )
  validates :medicine, :channel, presence: true

  # indexes

  scope :valid, -> { where(deleted_at: nil) }

  mount_uploader :image, ImageUploader

  def image_url
    image.try(:url)
  end

  def self.all_otc?(ids)
    medicine_ids = Product.where(:id.in => Array(ids)).pluck(:medicine_id)
    Medicine.where(:id.in => Array(medicine_ids)).pluck(:otc).all?
  end

  def otc
    medicine.try(:otc)
  end

  def price(area_id = nil)
    return self.prices.first.try(:price) if area_id.blank?
    self.prices.where(area_id: area_id).first.try(:price)
  end

  def name_and_specification
    String(self.read_attribute(:name)) + "  " + String(specification)
  end

  # def price=(_price)
  #   _p = self.prices.first
  #   if _p.present?
  #     _p.price = _price
  #     _p.save
  #   else
  #     self.prices.create(price: _price, area: Area.first)
  #   end
  # end

end
