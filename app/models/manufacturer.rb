class Manufacturer
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name
  field :image
  has_many :addresses, as: :addressable
  
  # author: depp
  has_many :medicines

  index name: 1

  mount_uploader :image, ImageUploader
end
