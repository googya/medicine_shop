class Inventory
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :product
  belongs_to :drug_store

  field :quantity#库存数量

end
