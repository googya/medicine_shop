#encoding: utf-8
require_relative '../../lib/const_set_ext'
require_relative '../../lib/service_object/coupon_code'

class Coupon
  include Mongoid::Document
  include Mongoid::Timestamps

  field :code, type: String
  field :description, type: String
  field :valid_from, type: DateTime
  field :valid_until, type: DateTime
  field :mode, type: String, default: 'amount' # 优惠类型， 比例或者金额
  field :amount, type: Integer
  field :genus, type: String # 优惠活动的类型(注册优惠， 通用优惠)

  STATE = {0 => '未使用', 1 => '已使用', 2 => '退款中'}
  field :state, type: Integer, default: 0
  field :redeemed_at, type: DateTime

  my_const_set('GENUS', [ :REG, :COMMON ])

  validates_inclusion_of :genus, in: GENUS
  validates_presence_of :valid_from

  belongs_to :creator, class_name: 'AdminUser', index: true
  has_one :coupon_redemption

  belongs_to :exchange_code
  belongs_to :patient
  has_one :order

  index code: 1
  index mode: 1
  index genus: 1
  index amount: 1


  before_create :setup_new
  def setup_new
    self.code = ::CouponCode.generate(parts: 1)
  end

  def expired?
    valid_until && valid_until <= Date.current
  end

end