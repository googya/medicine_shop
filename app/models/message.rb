#encoding: utf-8
require_relative '../../lib/const_set_ext'

class Message

  include Mongoid::Document
  include Mongoid::Timestamps

  my_const_set('ACTIONS', [
      :NEW_ORDER, :NEW_ACTIVITY ])

  field :is_read, type: Boolean, default: false
  field :deleted_at, type: DateTime
  field :read_at, type: DateTime
  field :action, type: String # 消息的名称

  belongs_to :user, class_name: 'Patient', index: true

  belongs_to :target, polymorphic: true, index: true

  scope :unread, -> { where(is_read: false) }
  scope :new_orders, -> { where(action: NEW_ORDER) }
  scope :new_activities, -> {  where(action: NEW_ACTIVITY) }

  # indexes
  index is_read: 1
  index read_at: 1

end