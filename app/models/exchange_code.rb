# encoding: utf-8
class ExchangeCode
  include Mongoid::Document
  include Mongoid::Timestamps

  field :code, type: String
  field :max_count, type: Integer, default: 10
  field :current_count, type: Integer, default: 0

  # genre Reg => 0, Spread => 1, Activity => 2
  field :genre, type: Integer

  belongs_to :patient, index: true
  has_many :coupons

  has_many :redeem_logs, :class_name => 'Logs::ExchangeCode'

  # indexes
  # index price: 1

  def self.check_invite_code(invite_code, patient_id)
    check_invite_code_flag = true
    exchange_code = ExchangeCode.where(code: invite_code).first
    if exchange_code && exchange_code.current_count < exchange_code.max_count

      exchange_code.current_count += 1
      exchange_code.save

      # generate redeem log
      exchange_code.redeem_logs.create(:patient_id => patient_id)
    else
      check_invite_code_flag = false
    end
    return check_invite_code_flag
  end

  def to_hash
    {
      id: String(_id),
      code: String(code),
      max_count: String(max_count),
      current_count: String(current_count),
      genre: String(genre),
      patient_id: String(patient_id),
    }
  end
end