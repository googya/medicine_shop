# encoding: utf-8
# 科室
class Department
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,          type: String

  validates :name, presence:   true
  validates :name, uniqueness: true
  index name: 1

  def to_hash
    {
      id:   id,
      name: name
    }
  end

  def self.init
    eles = ["感染科", "风湿科 ", "肿瘤内科", "普外科或肿瘤外科", "消化科", "儿科", "呼吸科", "神经外科", "普外科", "妇科", "骨科", "急诊", "肾内科", "透析室", "老年科", "风湿科", "内分泌科", "肿瘤科", "药剂科", "血液科"]
    eles.each do |name|
      self.create(name: name)
    end
  end
end
