#encoding: utf-8
require_relative '../../lib/service_object/coupon/yield_coupon'
class Invitation

  include Mongoid::Document
  include Mongoid::Timestamps

  field :other_sender_user, type: String # 备用， 通过其他方式创建的邀请码

  belongs_to :sender, class_name: 'Patient', inverse_of: 'sent_invitations', index: true
  belongs_to :receiver, class_name: 'Patient', inverse_of: 'received_invitation', index: true

end