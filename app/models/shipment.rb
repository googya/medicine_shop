class Shipment
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :address
  belongs_to :order
  field :ship_time, type: DateTime
  field :shipping_method

  # has_one :shipping_method
  
end