# -*- coding: utf-8 -*-
require_relative '../../lib/service_object/gen_ticket'
class Doctor < User

  include Mongoid::SoftDelete

  field :practicing_certificate
  field :birthday
  field :department#科室
  field :title#职称
  field :grade#级别
  field :ticket # 微信上对应的 ticket 可用于获取二维码图片
  field :doc_license 

  #belongs_to hospital
  #has_many :patients
  #field :qr_code #作为一个方法实现

  # authoer: depp
  belongs_to :hospital, index: true
  belongs_to :province, class_name: 'Area', index: true
  belongs_to :city,     class_name: 'Area', index: true
  has_many :patients
  has_many :logs_scans, :class_name => 'Logs::Scan'
  has_one :prescription_doctor # 一个医生， 理论上只能对应一个处方医生

  validates :mobile, uniqueness: true
  validates :mobile, format: { with: /\A0?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}\z/ } # 手机号码验证

  index ticket: 1
  index doc_license: 1

  mount_uploader :qr_code, ImageUploader
  
  mount_uploader :doc_license, ImageUploader

  def ticket_qr_code
    return nil if self.ticket.blank?
    "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=#{self.ticket}"
  end

  def to_hash
    {
        id: _id.to_s,
        username: username,
        token: private_token,
        sin: sin,
        mobile: mobile,
        status: status,
        avatar: avatar_url_with_host
    }
  end

  def my_patients
    Patient.where(:id.in => Array( my_prescriptions.pluck(:patient_id) ))
  end

  def my_prescriptions
    prescription_doctor.prescriptions
  end

  def prescriptions_for(user)
    return Prescription.none if prescription_doctor.blank?
    prescription_doctor.prescriptions.where(:patient_id.in => Array(user))
  end


  private
  before_create :gen_ticket
  def gen_ticket
    _ticket = ::GenTicket.new(self.id.to_s).gen_ticket
    self.ticket = _ticket
  end
end
