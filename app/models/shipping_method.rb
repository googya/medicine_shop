class ShippingMethod
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name

  belongs_to :shipment

end