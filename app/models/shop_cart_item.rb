class ShopCartItem
  include Mongoid::Document

  field :quantity, type: Integer, default: 0
  field :price, type: Float

  belongs_to :product, index: true

  embedded_in :shop_cart

  validates :quantity, numericality: { greater_than_or_equal_to: 0 }
  delegate :name, :image_url, to: :product, prefix: true

  # indexes
  index quantity: 1
  index price: 1


  def jbuild(*args, &block)
    Jbuilder.new(*args, &block).attributes!
  end

  def to_builder
    jbuild do |json|
      json.quantity self.quantity
      json.price self.price
      json.product_id String(self.product_id)
      json.product_name String(self.product_name)
      json.product_image_url String( self.product.try(:medicine).try(:image_url) )
      json.product_otc self.product.try(:otc)
    end
  end
end
