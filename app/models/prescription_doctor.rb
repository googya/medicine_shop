# -*- coding: utf-8 -*-
# 处方医生，医生临时信息表
require_relative '../../lib/service_object/gen_ticket'
require_relative '../../app/models/mongoid/soft_delete'
class PrescriptionDoctor
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::SoftDelete

  field :name,           type: String
  field :ticket # 微信上对应的 ticket 可用于获取二维码图片

  belongs_to  :hospital,    class_name: 'Hospital', index: true
  has_many    :logs_scans,  class_name: 'Logs::Scan'
  has_many    :prescriptions
  belongs_to  :doctor, index: true # 从属于一个医生
  belongs_to  :department, index: true

  # indexes
  index name: 1
  index ticket: 1

  def ticket_qr_code
    return nil if self.ticket.blank?
    "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=#{self.ticket}"
  end

  def hospital_name
    hospital.try(:name)
  end

  def hospital_address
    hospital.try(:address)
  end

  def prescriptions_count
    prescriptions.count
  end

  def patients_count
    prescriptions.pluck(:patient_id).uniq.count
  end


  private
  before_create :gen_ticket
  def gen_ticket
    _ticket = GenTicket.new(self.id.to_s).gen_ticket rescue ''
    return if _ticket.blank?
    self.ticket = _ticket
  end
end
