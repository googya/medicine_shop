# -*- coding: utf-8 -*-
require_relative '../../lib/service_object/coupon/yield_coupon'

class Patient < User

  ActiveValidators.activate(:sin, :id_num)

  # attr_accessor :invitation_code # 通过改邀请码注册来的

  field :id_num
  field :birthday
  field :address
  # 微信, 扫码生成, 密码统一123456
  field :open_id
  # field :medical_report_card
  field :invite_code, type: String # 认证成功之后， 可生成一个邀请码
  
  # mount_uploader :medical_report_card, ImageUploader


  #关联医生
  #处方记录
  #订单记录
  #field :qr_code #作为一个方法实现, 其 id 的表示

  # author: depp
  belongs_to :hospital, index: true
  belongs_to :province, class_name: 'Area'
  belongs_to :city,     class_name: 'Area'
  belongs_to :doctor, index: true
  has_many :orders
  has_one :shop_cart # 一个用户仅能有一个 shop_cart
  has_many :prescriptions
  has_one :device, as: :devicable # 设备
  has_many :addresses, dependent: :destroy,  as: :addressable
  has_many :messages
  has_one  :user_message_read, class_name: 'User::MessageRead'
  has_many :sent_invitations, class_name: 'Invitation', inverse_of: 'sender' # 发出去的（多个）
  has_one  :received_invitation, class_name: 'Invitation', inverse_of: 'receiver'  # 收到的邀请（仅限一个）

  delegate :value, to: :device, prefix: true, allow_nil: true# 用户的设备号


  # events-begin
  has_many :prescription_events, class_name: 'Logs::Prescription', as: :accountable
  has_many :order_events, class_name: 'Logs::Order', as: :accountable
  # events-end

  #has_many :pharmacist_records

  # need fix
  # has_many :coupon_redemptions

  has_many :coupons
  has_one :exchange_code

  # validates
  validates :mobile, format: { with: /\A0?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}\z/ } # 手机号码验证
  validates_uniqueness_of :mobile
  validates_uniqueness_of :invite_code, :allow_blank => true, :allow_nil => true

  # indexes
  index sin: 1
  index invite_code: 1
  index private_token: 1


  mount_uploader :qr_code, ImageUploader
  mount_uploader :avatar, AvatarUploader


  # callbacks
  before_save do
    if self.changes['status'].present?
      _s_old, _s_new =  Array(changes['status']).map(&:to_s)
      if _s_old == 'id_unrecognized' && _s_new == 'id_recognized'
        gen_invitation_code
        coupons_for_sender # 认证成功， 邀请者可获三张优惠券
        gen_common_coupon  # 认证成功， 可获通用优惠券
      end
    end
  end

  # after_create :gen_reg_coupon
  after_create :gen_exchange_code

  # callbacks


  # class method

  class << self
    def generate_code(code_length=6)
      chars = ("a".."z").to_a + ("1".."9").to_a
      new_code = Array.new(code_length, '').collect { chars[rand(chars.size)] }.join
      Digest::MD5.hexdigest(new_code)[0..(code_length-1)].upcase
    end

    def generate_unique_code
      begin
        new_code = generate_code
      end until !active_code?(new_code)
      new_code
    end

    def active_code?(code)
      where(invite_code: code).first
    end
  end


  # class method end

  def to_hash(options = {})
    {
        id: _id.to_s,
        username: username,
        token: private_token,
        sin: sin,
        mobile: mobile,
        status: status,
        role: role,
        avatar: avatar_url_with_host,
        invite_code: invite_code
    }.with_indifferent_access.except(*options.keys)

  end

  def default_shipping_address
    addresses.where(default: true).first
  end

  def set_default_address(address)
    unset_all_default_addresses
    address.update_attribute(:default, true)
  end

  def unset_all_default_addresses
    addresses.update_all(default: false)
  end


  def prescription_doctors
    PrescriptionDoctor.where( :id.in => Array( self.prescriptions.distinct(:prescription_doctor_id ) ) ).order_by(created_at: :desc)
  end

  # 未区分是否过期 15-6-8
  def coupons
    Coupon.in(id: coupon_redemptions.valid.pluck(:coupon_id) )
  end

  def gen_invitation_code
    self.invite_code = self.class.generate_unique_code
  end

  private
  def gen_exchange_code
    ExchangeCode.create(code: SecureRandom.hex(3).upcase, genre: 0, patient_id: self.id)
  end

  def gen_reg_coupon
    ::Coupon::YieldCoupon.new( Coupon::REG, 100 ).assign_to( self )
  end

  def gen_common_coupon
    _coupon = Coupon.create(amount: 20, genus: Coupon::COMMON, valid_from: Time.now)
    self.coupon_redemptions << CouponRedemption.create(coupon: _coupon)
  end

  def coupons_for_sender
    _ri = received_invitation
    return if _ri.blank?
    # 认证成功后， 邀请人获得三张通用优惠券 100, 50, 20
    if _ri.sender.present?
      [100, 50, 20].each do |amount|
        ::Coupon::YieldCoupon.new( Coupon::COMMON, amount ).assign_to( _ri.sender )
      end
    end
  end

end
