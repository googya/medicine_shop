# -*- coding: utf-8 -*-

class Hospital
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name
  field :standard_id
  field :address                # 可以为地址对象
  field :grading                # 级别
  field :phone
  field :lat                    # 纬度
  field :lng                    # 经度
  field :department_ids, default: []    # 科室列表

  #TODO 医院的编辑者为何

  # author: depp
  field :image #医院图片

  belongs_to :province, class_name: 'Area', inverse_of: :province_hospitals
  belongs_to :city,     class_name: 'Area', inverse_of: :city_hospitals

  has_many :doctors
  # has_many :endings, as: :portable
  has_many :prescription_doctors, class_name: 'PrescriptionDoctor'

  index name: 1
  index department_ids: 1

  mount_uploader :image, ImageUploader

  def provincex
    province.name
  end

  def gps
    lat.to_s + '/' + lng.to_s
  end
end
