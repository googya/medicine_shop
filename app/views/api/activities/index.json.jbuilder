json.status :ok
json.msg 'list'
json.activities @activities do |activity|
  json.id activity.id.to_s
  json.(activity, :content, :link, :manner, :title)
  json.image activity.image.try(:url)
end