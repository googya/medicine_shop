json.status :ok
json.msg :show_ok
json.product do
  json.partial! 'api/products/product', product: @product
end
