json.(product, :name, :identifier, :indication, :price, :alias_name, :description, :manufacturer_name, :specification, :unit, :dosage_form, :otc)
json.id product.id.to_s
json.description product.medicine.description
json.image product.medicine.try(:image_url)
