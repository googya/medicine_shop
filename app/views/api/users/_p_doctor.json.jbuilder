json.doctor do
  json.id p_doctor.id.to_s
  json.name p_doctor.name
  json.hospital do
    json.id String( p_doctor.hospital_id )
    json.name String(  p_doctor.hospital.try(:name)  )
    json.address String(  p_doctor.hospital.try(:address)  )
  end
  json.department p_doctor.department.try(:name)
end
