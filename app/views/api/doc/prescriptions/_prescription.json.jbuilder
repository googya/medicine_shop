json.(prescription, :note, :declined_reason, :created_at, )
json.id prescription.id.to_s
json.image prescription.image_url
json.status_info do
  json.status prescription.status
  json.status_message chinese_status(prescription.status)
end

json.products @line_items[prescription.id.to_s] do |item|
  json.id String(item.product_id)
  json.price item.price
  json.quantity item.quantity
  json.image item.product.try(:medicine).try(:image_url)
end

json.hospital do
  json.id String(prescription.hospital.try(:id))
  json.name prescription.hospital.try(:name)
  json.address prescription.hospital.try(:address)
  json.department prescription.department.try(:name)
end

json.patient  do
  json.id String( prescription.try(:patient).try(:id) )
  json.name prescription.try(:patient).try(:username)
  json.avatar prescription.try(:patient).try(:avatar_url_with_host)
end