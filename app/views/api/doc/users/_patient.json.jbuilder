json.(patient, :username, :sin, )
json.avatar patient.avatar_url_with_host
json.id String(patient.id)

# json.prescriptions @prescription_hash[String(patient.id)] do |prescription|
#   render partial: 'api/doc/prescriptions/prescription', prescription: prescription
# end


json.prescriptions @prescription_hash[String(patient.id)],  partial: 'api/doc/prescriptions/prescription', as: :prescription