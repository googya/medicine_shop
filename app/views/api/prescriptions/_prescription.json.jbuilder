json.(prescription, :note, :declined_reason, :created_at, )
json.id prescription.id.to_s
json.image prescription.image_url
json.status chinese_status(prescription.status)
json.area_id String(prescription.area_id)
json.status_info do
  json.status prescription.status
  json.status_message chinese_status(prescription.status)
end
