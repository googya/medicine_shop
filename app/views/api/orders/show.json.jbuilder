json.status :ok
json.msg :show

json.state @order.state
json.total @order.total

json.line_items @order.line_items do |item|
  json.id item.id.to_s
  json.(item, :price, :quantity)
  json.product_name item.product.try(:name)
  json.product_image item.product.try(:medicine).try(:image_url)
  json.product_id item.product.id.to_s
  json.product_otc item.product_otc
  json.is_deleted item.is_deleted
  json.max item.max
end

# 处方上的信息
json.prescription_items Array(@order.prescription.try(:pres_items)) do |pres_item|
  json.(pres_item, :price, :quantity)
  json.product_name pres_item.product.try(:name)
  json.product_image pres_item.product.try(:image_url)
  json.product_id pres_item.product.id.to_s
  json.product_otc pres_item.product.try(:otc)
end

# 地址信息
json.address do |addr|
    if @order.ship_address.present?
      json.state @order.ship_address.try(:state)
      json.city @order.ship_address.try(:city)
      json.phone @order.ship_address.try(:phone)
      json.address @order.ship_address.try(:address1)
      json.shipment_method @order.shipment_method.try(:name)
      json.name @order.ship_address.try(:name)
      json.id String(@order.ship_address.try(:id))
    elsif @order.temp_shipment_address.present?
      json.address @order.temp_shipment_address
    else
      json.state @patient.default_shipping_address.try(:state)
      json.city @patient.default_shipping_address.try(:city)
      json.phone @patient.default_shipping_address.try(:phone)
      json.address @patient.default_shipping_address.try(:address1)
      json.shipment_method @order.shipment_method.try(:name)
      json.name @patient.default_shipping_address.try(:name)
      json.id String(@patient.default_shipping_address.try(:id))
    end
end

json.shipment_method @order.shipment_method
json.payment_method @order.payment_method
json.ship_time @order.ship_time
json.pres_image @order.prescription_image_url

# 医生信息
json.doctor do
  json.department @order.prescription.try(:department_name)
  json.name @order.prescription.try(:prescription_doctor_name)
  json.hospital do
    json.name @order.prescription.try(:prescription_doctor_hospital_name)
    json.address @order.prescription.try(:prescription_doctor_hospital_address)
  end
end
