json.doctor_name order.doctor.try(:name)
json.hospital_name order.hospital.try(:name)
json.total order.total
json.id String(order.id)
json.state order.state
json.line_items order.line_items do |item|
  json.id item.id.to_s
  json.(item, :price, :quantity)
  json.product_name item.product_name
  # json.product_image item.product_image_url
  json.product_image item.product.try(:medicine).try(:image_url)
  json.product_id String(item.product_id)
  json.product_desc item.product_description
  json.product_otc item.product_otc
end
json.pres_image order.prescription_image_url