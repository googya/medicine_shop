$(document).ready(function(){
  //chosen js begin
  $('.chosen-select').chosen({
    no_results_text: '没有相应结果：',
    allow_single_deselect: true
  });
  //chosen js end

  //validate js begin
  $.validator.setDefaults({ ignore: ":hidden:not(select)" });
  $('.valForm').validate({
    submitHandler: function(form) {
      $(form).find(':submit').attr('disabled', true);
      form.submit();
    },
    highlight: function(element) {
      $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element.next());
      }
    }
  });
  //validate js end

  //prescription js begin
  // $('#prescription-approval-btn').click(function () {
  //   $('#prescription_status').val('approved');
  //   var data_j = [];
  //   $('.pres-item-wrapper').each(function (index, element) {
  //     var item =  $(element).find('.standardSelect option:selected');
  //     var quantity = $(element).find('input[type=number]');
  //     if(item && quantity){
  //       data_j.push({product_id: item.val(), quantity: quantity.val() });
  //     }
  //   });

  //   $('#pres_items_attributes').val( data_j );

  //   $('.prescription-edit-form').submit();
  // });

  // $('#add-pres-item').click(function () {
  //   var len = $('.pres-item-wrapper').length;

  //   var html = $('.pres-item-wrapper-temp').clone();
  //   html = html.html().replace(/INDEX/g, len);
  //   html = $(html);
  //   var $el = $("<div class='form-group pres-item-wrapper'></div>").html(html);
  //   $('.pres-item-list').append($el);

  //   $('.pres-item-wrapper').find('.productSelect').addClass('chosen-select');

  //   $(".chosen-select").chosen({no_results_text: "没有相应结果："});

  //   fetchItemData(html);

  // $('.pres-item-wrapper').each(function (index, element) {
  //  fetchItemData($(element));
  // });

  // $('body').on('click', '.delete-pres-item', function () {
  //   var $root = $(this).closest('.pres-item-wrapper');
  //   var $destroy_input = $root.find('.destroy-input');
  //   $destroy_input.val('true');
  //   $root.addClass('hidden');
  // });

  // $('#confirm-declined-btn').click(function () {
  //   $('#prescription_declined_reason').val($('#declined_reason').val());
  //   $('#prescription_status').val('declined');
  //   $('.prescription-edit-form').submit();
  // });

  // $('#prescription_hospital_id').change(function () {
  //   if ($(this).val() != '') {
  //     var h_id = $("#prescription_hospital_id").val();
  //     var d_id = $("#prescription_department_id").val();
  //     $.ajax({
  //       method: "GET",
  //       url: 'get_doctors',
  //       data: {
  //         hospital_id: h_id,
  //         department_id: d_id
  //       },
  //       success: function (data) {
  //         $(".select_doctors").html(data)
  //       }
  //     });
  //   }
  // });
  //prescription js end

  //image rotate js begin
  var param = {
    right: $("#rotRight"),
    left: $("#rotLeft"),
    box: $("#imgBox"),
    rot: 0,
    img_url: $("#rotImg").attr("src")
  };
  var fun = {
    right: function(){
      param.rot += 90;
      $("#rotImg").rotate(param.rot);
      if(param.rot === 270){
        param.rot = -90;
      }
      return false;
    },
    left: function(){
      param.rot -= 90;
      if(param.rot === -90){
        param.rot = 270;
      }
      $("#rotImg").rotate(param.rot);
      return false;
    }
  };
  param.right.click(function(){
    if(!$("img#rotImg").length){
      param.box.html('<img id="rotImg" src="' + param.img_url + '" />');
    }
    fun.right();
    return false;
  });
  param.left.click(function(){
    if(!$("img#rotImg").length){
      param.box.html('<img id="rotImg" src="' + param.img_url + '" />');
    }
    fun.left();
    return false;
  });
  //image rotate js end

});

// function fetchItemData(element){
//   var name = element.find(".productSelect option:selected").text();
//   $.ajax({
//     url: '/admin/v2/products/filter_by',
//     method: 'get',
//     data: {name: name}
//   }).done(function (data) {
//     renderItem(element,data);
//   });
// }

// function renderItem(element,data){
//   var select = element.find('.standardSelect');
//   select.empty();
//   var pro_select = element.find('.productSelect');
//   var itemPrice = element.find('.priceInput');
//   var itemQuantity = element.find('.qInput');
//   pro_select.unbind('change');
//   pro_select.change(function () {
//     fetchItemData(element);
//   });
//   select.unbind('change');
//   select.change(function () {
//     itemPrice.val($(this).find('option:selected').data('price'));
//   });
//   $(data['products']).each(function (index, ele) {
//     select.append("<option value='" + ele['product_id'] + "' data-price='" + ele['price'] + "'> " + ele['specification'] + "</option>");
//     itemPrice.val(select.find('option:selected').data('price'));
//     itemQuantity.val('');
//   })
// }
