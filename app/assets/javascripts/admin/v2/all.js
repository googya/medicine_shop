// Mainly scripts
//= require bsthemes/jquery-2.1.1
//= require bsthemes/bootstrap.min
//= require bsthemes/plugins/metisMenu/jquery.metisMenu
//= require bsthemes/plugins/slimscroll/jquery.slimscroll.min
// Flot
//= require bsthemes/plugins/flot/jquery.flot
//= require bsthemes/plugins/flot/jquery.flot.tooltip.min
//= require bsthemes/plugins/flot/jquery.flot.spline
//= require bsthemes/plugins/flot/jquery.flot.resize
//= require bsthemes/plugins/flot/jquery.flot.pie
// Peity
//= require bsthemes/plugins/peity/jquery.peity.min
//= require bsthemes/demo/peity-demo
// Custom and plugin javascript
//= require bsthemes/inspinia
//= require bsthemes/plugins/pace/pace.min
// jQuery UI
//= require bsthemes/plugins/jquery-ui/jquery-ui.min
// GITTER
//= require bsthemes/plugins/gritter/jquery.gritter.min
// EayPIE
//= require bsthemes/plugins/easypiechart/jquery.easypiechart
// Sparkline
//= require bsthemes/plugins/sparkline/jquery.sparkline.min
// Sparkline demo data
//= require bsthemes/demo/sparkline-demo
// ChartJS
//= require bsthemes/plugins/chartJs/Chart.min
// Chosen
//= require bsthemes/plugins/chosen/chosen.jquery
// Datapicker
//= require bsthemes/plugins/datapicker/bootstrap-datepicker
// Validate
//= require bsthemes/plugins/validate/jquery.validate.min

// pages custom js
//= require admin/v2/jquery.rotate
//= require admin/v2/jquery.validate.messages_cn
//= require admin/v2/uploadPreview.min
//= require admin/v2/basic
//= require jquery_ujs
//= require cocoon
