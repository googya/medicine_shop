class Admin::V2::Weixin::ProductsController < Admin::V2::BaseController
  before_action :set_channel

  def index
    @products = @channel.products.valid.page(params[:page] || 1)
  end

  def show
    @product = Product.find(params[:id])
  end

  def create
    product = Product.new(product_params)
    product.channel = set_channel
    if product.save
      redirect_to admin_v2_weixin_products_path
    else
      render new_admin_v2_weixin_product_path
    end
  end

  def edit
    get_medicines
    @product = Product.find(params[:id])
  end

  def new
    @product = Product.new
    get_medicines
  end

  def update
    product = Product.find(params[:id])
    product.update_attributes(product_params)
    if product.save
      redirect_to admin_v2_weixin_products_path
    else
      render :edit
    end
  end

  def destroy
    product = Product.find(params[:id])
    product.update_attribute(:deleted_at, Time.now)
    redirect_to admin_v2_weixin_products_path
  end

  protected

  def product_params
    params.require(:product).permit(:name, :price, :medicine_id)
  end

  def set_channel
    @channel ||= Channel.find_or_create_by(:name => 'weixin')
  end

  def get_medicines
    @medicines ||= Medicine.all
  end
end
