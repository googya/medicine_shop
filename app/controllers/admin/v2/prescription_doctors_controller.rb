# encoding: utf-8
class Admin::V2::PrescriptionDoctorsController < Admin::V2::BaseController

  def index
    @pds = PrescriptionDoctor.all.desc("created_at")
  end

  def new
    @pd = PrescriptionDoctor.new
  end

  def edit
    @pd = PrescriptionDoctor.find(params[:id])
  end

  def update
    doc = PrescriptionDoctor.find(params[:id])
    doc.update_attributes(prescription_doc_params)
    if doc.save
      redirect_to admin_v2_prescription_doctors_path
    else
      render :edit
    end
  end

  def create

  end

  def destroy
    doc = PrescriptionDoctor.find(params[:id])
    doc.destroy
    redirect_to admin_v2_prescription_doctors_path, notice: '删除成功'
  end

  def prescription_doc_params
    params.require(:prescription_doctor).permit(:name, :hospital_id)
  end

end
