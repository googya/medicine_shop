class Admin::V2::PrescriptionsController < Admin::V2::BaseController
  before_action :fetch_prescription, only: [:edit, :update, :destroy]
  before_action :products_in_zones, only: [ :edit ]
  load_and_authorize_resource

  def index
    %i[pending approved declined].each_with_object( @prescriptions = {} ) do |s_type, hash|
      hash[s_type] = prescriptions_in_current_zones.send(s_type).\
          desc("updated_at").page(params[:page]||1).per(20)
    end
  end

  def edit
    @pres_items       = @prescription.pres_items
    @p_hospital_id    = @prescription.hospital_id
    @p_department_id  = @prescription.department_id
    @doctors          = @prescription.hospital ? @prescription.hospital.prescription_doctors : []
    @channel.products.each_with_object(@product_infos = {}) do |e, hash|
      hash[e.name] ||= e.id.to_s
    end

    # 按处方提交城市给对应的医院
    @hospitals = @prescription.area ? Hospital.where(city_id: @prescription.area_id) : Hospital.all
  end

  def update
    prescription_doctor = PrescriptionDoctor.where(id: params[:prescription][:prescription_doctor_id]).first

    unless prescription_doctor
      prescription_doctor = PrescriptionDoctor.create(name: params[:prescription][:prescription_doctor_id], hospital_id: params[:prescription][:hospital_id])
      # prescription_params[:prescription_doctor_id] = prescription_doctor.id
    end

    # 自定义科室
    department = Department.where(id: params[:prescription][:department_id]).first
    department = Department.create(name: params[:prescription][:department_id]) unless department

    @prescription.update_attributes(prescription_params)
    @prescription.prescription_doctor_id = prescription_doctor.id
    @prescription.department_id = department.id
    prescription_doctor.department =  department
    if @prescription.save
      @prescription.send_to_wechat # 如果该处方来自微信的话需要推一个消息给该处方的open_id
      redirect_to admin_v2_prescriptions_path, notice: '已保存'
    else
      render :edit, alert: @prescription.errors.full_messages.join(', ')
    end
  end

  def get_doctors
    @prescription     = Prescription.where(id: params[:id]).first
    hospital          = Hospital.where(id: params[:hospital_id]).first
    @doctors          = hospital ? hospital.prescription_doctors : []

    render partial: 'select_doctor'
  end

  def destroy
    @prescription.destroy
    redirect_to admin_v2_prescriptions_path, notice: '删除成功'
  end

  private
  def fetch_prescription
    begin
      @prescription ||= Prescription.find(params[:id])
    rescue
      redirect_to admin_v2_prescriptions_path, notice: '处方不存在'
    end
  end

  def prescription_params
    params.require(:prescription).permit(:status, :declined_reason, :hospital_id, :department_id, :pres_date, :diagnosis, :prescription_doctor_id, :pres_items_attributes => [:id, :product_id, :quantity, :_destroy, :price])
  end

  def prescriptions_in_current_zones
    @all_prescriptions ||= if current_admin_user.super_admin?
                      Prescription.all
                    else
                      Prescription.in_zones( current_zones )
                    end
  end


  def products_in_zones
    # @products ||= if current_admin_user.super_admin?
    #                          @channel.products
    #                        else
    #                          @channel.products.where( :id.in => Price.where( :area_id => @prescription.price_area ).pluck(:product_id) )
    #                        end
    @products = Product.where(:id.in => Price.where(:area_id =>@prescription.price_area).pluck(:product_id))
  end

  # def set_area
  #   @area ||= if current_admin_user.super_admin?
  #             Area.provinces.where( name: '上海' ).last
  #           else
  #             current_admin_user.zones.last
  #           end
  # end

end
