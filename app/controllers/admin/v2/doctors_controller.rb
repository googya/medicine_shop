# encoding: utf-8

class Admin::V2::DoctorsController < Admin::V2::BaseController
  
  def index
    conditions = {}
    conditions.merge!({province_id: params[:province_id]}) if params[:province_id]
    conditions.merge!({city_id: params[:city_id]}) if params[:city_id]
    conditions.merge!({hospital_id: params[:hospital_id]}) if params[:hospital_id]
    conditions.merge!({ks_id: params[:ks_id]}) if params[:ks_id]
    @doctors = Doctor.where(conditions).desc('created_at').page(params[:page]||1).per(20)
  end
  
  def show
    @doctor = Doctor.find params[:id]
  end
  
  def update_identity
    @doctor = Doctor.find params[:id]
    if @doctor.id_unrecognized?
      @doctor.identity_recon
    else
      @doctor.identity_delay
    end
    @doctor.save
    flash[:notice] = '操作成功！'
    
    redirect_to action: :index
  end
end