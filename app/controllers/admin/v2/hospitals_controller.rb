# encoding: utf-8

class Admin::V2::HospitalsController < Admin::V2::BaseController

  def index
    conditions = {}
    conditions.merge!({province_id: params[:province_id]}) if params[:province_id]
    conditions.merge!({city_id: params[:city_id]}) if params[:city_id]
    @hospitals = Hospital.where(conditions).desc('created_at').page(params[:page] || 1).per(20)
  end

  def new
    @hospital = Hospital.new
  end

  def edit
    @hospital = Hospital.find(params[:id])
  end

  def create
    hospital = Hospital.new(hospital_params)
    if hospital.save
      redirect_to admin_v2_hospitals_url
    else
      render new_admin_v2_hospital_url
    end
  end

  def update
    hospital = Hospital.find(params[:id])
    hospital.update_attributes(hospital_params)
    redirect_to admin_v2_hospitals_path
  end

  protected

  def hospital_params
    params.require(:hospital).permit(:name, :address, :grading, :phone, :city_id, :province_id, :department_ids)
  end

end
