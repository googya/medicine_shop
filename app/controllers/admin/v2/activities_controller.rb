class Admin::V2::ActivitiesController < Admin::V2::BaseController
  load_and_authorize_resource

  def index
    activities = @channel.activities.activity.order(updated_at: :desc).page( params[:page] || 1 )
    donates = @channel.activities.order(updated_at: :desc).page( params[:page] || 1 )
    @activities = { activity: activities, donate: donates }
  end

  def show
    @activity = Activity.find( params[:id] )
  end

  def update
    activity = Activity.find( params[:id] )
    activity.update_attributes( activity_params )
    activity.save!
      redirect_to admin_v2_activities_path
    # else
    #   render :edit
    # end
  end

  def create
    a = @channel.activities.new( activity_params )
    if a.save
      redirect_to admin_v2_activities_path
    else
      render :new
    end
  end

  def edit
    @activity = Activity.find( params[:id] )
  end

  def new
    @activity = Activity.new
  end

  def destroy
  end

  protected

  def activity_params
    params.require(:activity).permit(:title, :content, :link, :image, :manner)
  end
end
