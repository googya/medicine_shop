# encoding: utf-8
class Admin::V2::AdminUsersController < Admin::V2::BaseController
  before_filter :get_admin_user, only: [:edit, :update, :destroy, :reset_password]
  before_filter :get_areas_and_roles, only: [:new, :update, :edit, :create]

  def index
    @admin_users = AdminUser.where(:role.ne => 'super').page(params[:page] || 1).per(20).includes(:zones)
  end

  def new
    @admin_user = AdminUser.new
  end

  def create
    @admin_user = AdminUser.new(admin_user_params)
    if @admin_user.save
      flash[:notice] = "保存成功！"
      redirect_to admin_v2_admin_users_path
    else
      flash[:alert] = "保存失败，请重新编辑！"
      redirect_to new_admin_v2_admin_user_path
    end
  end

  def edit
    @admin_user = get_admin_user
  end

  def update
    @admin_user = get_admin_user
    if @admin_user.update_attributes(admin_user_params)
      flash[:notice] = "保存成功！"
      redirect_to admin_v2_admin_users_path
    else
      flash[:alert] = "保存失败，请重新编辑！"
      render :edit
    end
  end

  def reset_password
    @admin_user = get_admin_user
  end

  def destroy
    admin_user = get_admin_user
    admin_user.destroy
    flash[:notice] = "删除成功！"
    redirect_to admin_v2_admin_users_path
  end

  private
  def admin_user_params
    params.require(:admin_user).permit(:mobile, :password, :role, :email, :username, :zone_ids=>[])
  end

  def get_areas_and_roles
    @areas = Area.where(:name.in=>%w(上海 浙江省 江苏省), :level=>2)
    @roles = ["content", "audit", "logistics", "sale"]
  end

  def get_admin_user
    AdminUser.find(params[:id])
  end
end