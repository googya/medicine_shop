# encoding: utf-8

class Admin::V2::MedicinesController < Admin::V2::BaseController
  
  def index
    @medicines = Medicine.by_name(params[:search]).desc('created_at').page(params[:page] || 1).per(20)
  end

  def new
    @medicine = Medicine.new
  end

  def edit
    @medicine = Medicine.find(params[:id])
  end

  def update
    @medicine = Medicine.find(params[:id])
    @medicine.update_attributes(medicine_params)
    redirect_to admin_v2_medicines_path
  end

  def create
    medicine = Medicine.new(medicine_params)
    if medicine.save
      redirect_to admin_v2_medicines_url
    else
      render new_admin_v2_medicine_path
    end
  end

  private

  def medicine_params
    params.require(:medicine).permit(:identifier, :barcode, :name,
                                     :common_name, :e_name, :pack, :dosage_form,
                                     :indication, :prohibited_combination, :description,
                                     :desc_img, :image, :is_health_insurance, :term_of_validity,
                                     :registered_id, :registration_period, :storage_condition,
                                     :cold_chain, :basic_drugs, :otc, :high_price,
                                     :guiding_price, :low_price, :specification, :unit, :serial_no)
  end
end