class Admin::V2::OrdersController < Admin::V2::BaseController
  skip_before_action :verify_authenticity_token, only: :pay
  load_and_authorize_resource

  def index
    %i(in_progress confirm paid unapproved).each_with_object( @orders = {} ) do |o_type, hash|
      t = orders_in_current_zones.send(o_type).desc("updated_at").page(params[:page]||1).per(20)
      hash[o_type] = t
    end
  end

  def show
    @order = Order.find(params[:id])
  end

  def pay
    order = Order.find(params[:id])
    order.state = Order::STATES[:paid]
    order.save

    payload = { order: order, accountable: order.patient }
    ActiveSupport::Notifications.instrument 'order.be_paid', payload

    render json: { status: :ok, msg: 'paid ok', order_state: order.state }
  rescue => e
    render json: { status: :not_ok, msg: e.message }
  end

  def destroy
    order = Order.find( params[:id] )
    order.destroy
    redirect_to admin_v2_orders_path, notice: '删除成功'
  end

  private
  def orders_in_current_zones
    @all_orders ||= if current_admin_user.super_admin?
                    Order.all
                  else
                    Order.where(:prescription_id.in => Prescription.in_zones(current_zones).pluck(:id))
                  end
  end
end
