# encoding: utf-8

class Admin::V2::DrugStoresController < Admin::V2::BaseController
  
  def index
    conditions = {}
    conditions.merge!({province: params[:province]}) if params[:province]
    conditions.merge!({city: params[:city]}) if params[:city]
    conditions.merge!({rank: params[:rank]}) if params[:rank]
    @drug_stores = DrugStore.where({}).desc('created_at').page(params[:page] || 1).per(20)
  end
end