# encoding: utf-8

class Admin::V2::UsersController < Admin::V2::BaseController
  load_and_authorize_resource

  def index
    conditions = {}
    conditions.merge!({province_id: params[:province_id]}) if params[:province_id]
    conditions.merge!({city_id: params[:city_id]}) if params[:city_id]
    @patients = Patient.where(conditions).desc('created_at').page(params[:page] || 1).per(20)
  end
  
  def show
    @patient = Patient.find params[:id]
  end
  
  def update_identity
    @patient = Patient.find params[:id]
    if @patient.id_unrecognized?
      @patient.identity_recon
    else
      @patient.identity_delay
    end
    @patient.save
    flash[:notice] = '操作成功！'
    
    redirect_to action: :index
  end
end