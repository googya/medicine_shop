class Admin::V2::DepartmentsController < Admin::V2::BaseController
  def index
    @departments = Department.order(updated_at: :desc).page(params[:page] || 1)
  end

  def new
    @department = Department.new
  end

  def edit
    @department = Department.find(params[:id])
  end

  def update
    @department = Department.find(params[:id])
    @department.attributes = department_params
    @department.save
    redirect_to admin_v2_departments_path
  end

  def create
    department = Department.find_or_initialize_by(name: department_params[:name])
    if department.save
      redirect_to admin_v2_departments_path
    else
      render 'new'
    end
  end

  def destroy
    Department.find(params[:id]).delete
    redirect_to admin_v2_departments_path
  end

  private

  def department_params
    params.require(:department).permit(:name)
  end


end