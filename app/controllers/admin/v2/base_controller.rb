class Admin::V2::BaseController < ApplicationController

  before_action :authenticate_admin_user!
  before_action :require_admin
  before_action :current_ability
  layout 'v2/admin'

  rescue_from CanCan::AccessDenied do |exception|
    if current_admin_user.audit_admin? || current_admin_user.try(:logistics_admin?)
      redirect_to admin_v2_orders_path  , :alert => exception.message
    else
      redirect_to admin_v2_banners_path  , :alert => exception.message
    end
  end

  private
  def require_admin
    redirect_to root_path unless  ( current_admin_user.try(:super_admin?) || current_admin_user.try(:content_admin?) ||
        current_admin_user.try(:audit_admin?) || current_admin_user.try(:logistics_admin?) )
  end

  def current_ability
    @ability ||= AdminAbility.new( current_admin_user )
  end

  def current_zones
    @zones ||= Area.where( :parent_id.in => Array( current_admin_user.zones) ).pluck(:id)
  end

end
