class Admin::V2::ProductsController < Admin::V2::BaseController

  load_and_authorize_resource

  def filter_by
    products = []
    if params[:name].present?
      _ps = @channel.products.where( :name => String(params[:name]) )
      _ps.each_with_object(products) do |product, object|
        item = { product_id: product.id.to_s, specification: product.medicine.try(:specification), price: product.try(:price) }
        object << item
      end
    end
    render json: { status: :ok, msg: 'ok', products: products }
  end

  def index
    @products = @channel.products.valid.page(params[:page] || 1)
  end

  def show
    @product = Product.find(params[:id])
  end

  def create
    product = ProductBusiness.build_product(
      attributes: product_params,
      channel: set_channel
    )

    if product
      redirect_to admin_v2_products_path
    else
      render new_admin_v2_product_path
    end
  end

  def edit
    get_medicines
    @product = Product.find(params[:id])
  end

  def new
    @product = Product.new
    get_medicines
  end

  def update
    product = Product.find(params[:id])
    product.update_attributes(product_params)
    if product.save
      redirect_to admin_v2_products_path
    else
      render :edit
    end
  end

  def destroy
    product = Product.find(params[:id])
    product.update_attribute(:deleted_at, Time.now)
    redirect_to admin_v2_products_path
  end


  def export_to_csv
    require 'csv'
    _products = Product.includes(:medicine)

    _csv = CSV.generate do |csv|
      csv << %w[ 商品编号 价格 通用名 商品名 规格 ]
      _products.each do |p|
        m = p.medicine
        csv << [ m.try(:serial_no), p.price,  m.try(:common_name), m.try(:name), m.try(:specification) ]
      end
    end

    send_data _csv, :type => 'text/csv; charset=iso-8859-1; header=present', :disposition => "attachment; filename=post-#{Time.now.strftime('%d-%m-%y--%H-%M')}.csv"
  end


  protected

  def product_params
    params.require(:product).permit(:name, :price, :medicine_id, prices_attributes: [ :area_id, :price, :_destroy, :id] )
  end


  def get_medicines
    @medicines ||= Medicine.all
  end


end
