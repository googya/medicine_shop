class Admin::AdminUser::SessionsController < ::Devise::SessionsController
  before_filter :configure_sign_in_params, only: [:create]
  # after_filter  :after_sign_out_path_for, only: [:destory]
  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  def create
    self.resource = warden.authenticate(auth_options)
    if self.resource
      sign_in(resource_name, resource)
      yield resource if block_given?
      respond_with resource, location: admin_v2_root_path
    else
      flash[:error] = '用户名 密码不匹配'
      redirect_to new_admin_user_session_path
    end

  end

  # DELETE /resource/sign_out
  def destroy
    super
  end

  protected

  # You can put the params you want to permit in the empty array.
  def configure_sign_in_params
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:mobile, :password, :remember_me, :remember_name ) }
  end

  # sign in after redirect to
  def after_sign_in_path_for(resource)
    request.referrer || '/'
  end

  # configure redirect to viewer after devise destroy
  def after_sign_out_path_for(resource_or_scope)
    '/'
  end
end