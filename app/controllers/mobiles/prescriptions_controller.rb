# encoding: utf-8
require_relative '../../../lib/weixin/weixin'

class Mobiles::PrescriptionsController < Mobiles::BaseController
  skip_before_filter :verify_authenticity_token

  before_action :gon_info
  before_filter :auth_wx, only: [:new_prescription]
  before_filter :get_wechat_user, only: [:new_prescription]

  def new_prescription
    @areas = Area.where(:name.in => ['上海', '南京', '杭州'], level: 3, :parent_id.ne => nil)
  end

  # params:  {"image_src"=>"6airlCBqxQwyulBr4JkrfCt-J1E616RSQBNtok-D6G6mijgctGHZYyh2i43q4f5l", "open_id"=>"oncQFs9dUoBXHBvXp7aWxPZwzwqc", "source_info"=>"wechat"}
  def create_prescription
    w_user = Patient.where(open_id: params[:open_id]).first

    area   = Area.where(id: params[:area_id]).first
    image_file = Weixin.get_pic(params[:image_src])

    pre             = Prescription.new
    pre.patient     = w_user
    pre.user_agent  = params[:source_info]
    pre.image       = File.open(image_file)
    pre.area        = area

    pre.save

    render json: {image_url: APP_CONFIG['host'] + pre.image_url}
  end

end
