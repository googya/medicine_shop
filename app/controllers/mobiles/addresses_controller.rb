# encoding: utf-8
class Mobiles::AddressesController < Mobiles::BaseController
  before_filter :get_patient
  skip_before_filter :verify_authenticity_token, only: [:destroy]

  def index
    @addresses = @user.addresses.desc(:updated_at)
  end

  def new
    @address = @user.addresses.new
  end

  def create
    address_hash = address_params
    address_hash[:state] = Area.where(id: address_params[:state]).first.try(:name)
    address_hash[:city]  = Area.where(id: address_params[:city]).first.try(:name)

    @user.addresses.create(address_hash)

    redirect_to Weixin.generate_oauth_path("#{APP_CONFIG['host']}/mobiles/users/my_addresses")
  end

  def edit
    @address = @user.addresses.where(id: params[:id]).first
  end

  def update
    @address = @user.addresses.where(id: params[:id]).first
    address_hash = address_params
    address_hash[:state] = Area.where(id: address_params[:state]).first.try(:name)
    address_hash[:city]  = Area.where(id: address_params[:city]).first.try(:name)
    @address.update_attributes(address_hash)

    redirect_to Weixin.generate_oauth_path("#{APP_CONFIG['host']}/mobiles/users/my_addresses")
  end

  def destroy
    address = @user.addresses.where(id: params[:id]).first

    address.delete

    redirect_to Weixin.generate_oauth_path("#{APP_CONFIG['host']}/mobiles/users/my_addresses")
  end

  def set_default
    @address = @user.addresses.where(id: params[:id]).first

    @user.addresses.update_all(default: false)

    @address.update_attributes(default: true)

    redirect_to Weixin.generate_oauth_path("#{APP_CONFIG['host']}/mobiles/users/my_addresses")
  end

  private

  def get_patient
    @user = Patient.find(params[:user_id])
  end

  def address_params
    params.require(:address).permit(:name, :address1, :state, :city, :phone, :zip_code)
  end

end
