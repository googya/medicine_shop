# encoding: utf-8
class Mobiles::BaseController < ApplicationController
  layout 'application.mobile'

  def auth_wx
    if params[:code]
      url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=#{WECHAT_CONFIG['appid']}&secret=#{WECHAT_CONFIG['secret']}&code=#{params[:code]}&grant_type=authorization_code"

      result    = HTTPClient.get(url)
      @open_id  = JSON(result.body)['openid']

      logger.info "*" * 100
      logger.info @open_id
      logger.info "*" * 100

      raise '该用户不存在' unless @open_id
    else
      raise '该用户不存在'
    end

    rescue => e
      render template: "mobiles/notice_view", locals: {message: e.message}
  end

  def get_wechat_user
    @w_user = Patient.where(open_id: @open_id).first

    raise '该用户不存在' unless @w_user

    rescue => e
      render template: "mobiles/notice_view", locals: {message: e.message}
  end

end
