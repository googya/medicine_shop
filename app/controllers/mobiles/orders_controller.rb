# encoding: utf-8
require_relative '../../../lib/weixin/weixin'
require_relative '../../../lib/service_object/coupon/apply_coupon'

class Mobiles::OrdersController < Mobiles::BaseController
  before_action :gon_info
  before_filter :auth_wx, only: [:edit]
  before_filter :get_wechat_user, only: [:edit]
  before_filter :get_order

  def edit
    raise '您无权访问！' if @w_user != @order.patient

    rescue => e
      render template: "mobiles/notice_view", locals: {message: e.message}
  end

  def update
    params[:item][:line_items].each do |k, v|
      item = @order.line_items.find(k)
      item.update_attributes(quantity: v)
    end
    @order.confirm
    # add coupon use
    coupon = Coupon.where(id: params[:order][:coupon_id]).first
    @order.coupon = coupon and ::Coupon::ApplyCoupon.new(coupon, @order).apply if coupon

    @order.save

    payload = { order: @order, accountable: @order.patient }
    ActiveSupport::Notifications.instrument 'order.be_confirmed', payload
  end

  def show
  end

  # extends

  def canncel
    @order.cancel
    @order.save

    redirect_to Weixin.generate_oauth_path("#{APP_CONFIG['host']}/mobiles/users/my_orders")
  end

  def edit_shipment
    @locations = @order.patient.addresses.map(&:to_ary)
  end

  def update_shipment
    shipment_method = params[:order][:shipment_method1] && params[:order].delete('shipment_method1') if params[:order][:shipment_method1]
    shipment_method = params[:order][:shipment_method2] && params[:order].delete('shipment_method2') if params[:order][:shipment_method2]

    if params[:address]
      address   = Address.find(params[:address])
      ship_time = "#{params[:order][:ship_time]} #{params[:time_select]}"

      params[:order].delete('ship_time')

      @order.update_attributes(params[:order].permit!.merge(shipment_method: shipment_method, ship_address_id: address.id, ship_time: ship_time))
    else
       @order.update_attributes(params[:order].permit!.merge(shipment_method: shipment_method))
    end

    redirect_to Weixin.generate_oauth_path("#{APP_CONFIG['host']}/mobiles/orders/#{@order.id}/edit")
  end

  private

  def get_order
    @order = Order.find(params[:id])
  end

end
