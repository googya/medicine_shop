class Mobiles::ActivitiesController < Mobiles::BaseController
  def index
    @activities = Activity.desc('created_at').page(params[:pages] || 1).per(10)
  end
end