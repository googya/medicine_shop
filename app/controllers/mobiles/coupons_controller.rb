# encoding: utf-8

class Mobiles::CouponsController < Mobiles::BaseController

  before_filter :get_user
  before_filter :get_coupon

  def show
  end

  private

  def get_user
    @patient = Patient.find params[:user_id]
  end

  def get_coupon
    @coupon = Coupon.find params[:id]
  end

end
