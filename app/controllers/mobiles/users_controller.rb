# encoding: utf-8
require_relative '../../../lib/service_object/set_verify_code'
require_relative '../../../lib/service_object/apply_invitation'
require_relative '../../../lib/weixin/weixin'

class Mobiles::UsersController < Mobiles::BaseController
  skip_before_filter :verify_authenticity_token
  before_action :gon_info
  before_filter :auth_wx, only: [:new, :my_orders, :my_prescriptions, :valid, :my_coupons, :my_center, :my_addresses, :share_my_invitation_code, :valid_success]
  before_filter :get_wechat_user, only: [:my_orders, :my_prescriptions, :valid, :my_coupons, :my_center, :my_addresses, :share_my_invitation_code, :valid_success]
  # before_filter :get_w_user

  def new
    raise '您已经是系统用户，无需重复注册。' if Patient.where(open_id: @open_id).count > 0

    @user = User.new

    rescue => e
      render template: "mobiles/notice_view", locals: {message: e.message}
  end

  def relate_user
    @error_notice ||= params[:error]
    @open_id      = params[:open_id]
    @user         = User.new
  end

  def create
    if params[:relate_user]
      @user = Patient.where(mobile: params[:user][:mobile]).last
      if @user && @user.authenticate(params[:user][:password])
        @user.update_attributes(open_id: params[:open_id])
      else
        redirect_to relate_user_mobiles_users_path(open_id: params[:open_id], error: '请您重新输入正确的手机号跟密码。')
      end
    else
      @user = Patient.create(user_params.merge(open_id: params[:open_id]))

      @user.ensure_private_token!
      SetVerifyCode.used(@user.mobile)

      # ApplyInvitation.new(params[:invitation_code], @user).create_invitation if params[:invitation_code]
      if(params[:invitation_code])
        flag = ExchangeCode.check_invite_code(params[:invitation_code], @user.id)
        if(!flag)
          puts "============"
          puts "============"
          puts "Wechat端"
          puts "============"
          puts "============"
          raise(Errors::InvalidInvitationCode, '该兑换码已失效')
        end
      end
    end
    rescue Errors::InvalidInvitationCode => e
      render template: "mobiles/notice_view", locals: {message: '用户创建成功， 但邀请码不正确'}
  end

  def my_center
  end


  def my_orders
    @orders = @w_user.orders.valid.desc(:updated_at).group_by(&:state)
  end

  def my_prescriptions
    @pres = @w_user.prescriptions.desc(:updated_at).group_by(&:status)
  end

  def my_coupons
    @coupons = @w_user.coupon_redemptions.valid.map(&:coupon)
  end

  def my_addersses

  end

  def valid
  end

  def valid_success
  end

  def upload
    pu = Patient.where(open_id: params[:open_id]).first

    image_file = Weixin.get_pic(params[:image_src])

    pu.id_image  = File.open(image_file)  if params[:image_type] == 'id_image'
    pu.hic_image = File.open(image_file)  if params[:image_type] == 'hic_image'

    pu.save

    image = pu.send(params[:image_type].to_sym)

    render json: {image_url: "#{APP_CONFIG['host']}/#{image.url}"}
  end

  def share_my_invitation_code
  end

  private

  def get_w_user
    @w_user = Patient.first
  end

  def user_params
    params.require(:user).permit(:mobile, :password, :username, :open_id)
  end
end
