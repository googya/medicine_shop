module Api
  class Doc::BaseController < ApplicationController
    # protect_from_forgery with: :null_session
    protect_from_forgery with: :exception
    # before_action :set_resource, only: [:destroy, :show, :update]
    respond_to :json

    skip_before_action :verify_authenticity_token, if: :json_request?

    before_action :doc_token_valid

    unless Rails.application.config.consider_all_requests_local

      rescue_from Mongoid::Errors::InvalidFind, with: :no_document_error
      rescue_from Mongoid::Errors::DocumentNotFound, with: :no_document_error

      rescue_from Exception do |e|
        Rails.logger.error e.backtrace.join("\n")
        render json: {status: :not_ok, msg: e.message}
      end
    end

    private

    # Returns the resource from the created instance variable
    # @return [Object]
    def get_resource
      instance_variable_get("@#{resource_name}")
    end

    # Returns the allowed parameters for searching
    # Override this method in each API controller
    # to permit additional parameters to search on
    # @return [Hash]
    def query_params
      {}
    end

    # Returns the allowed parameters for pagination
    # @return [Hash]
    def page_params
      params.permit(:page, :page_size)
    end

    # The resource class based on the controller
    # @return [Class]
    def resource_class
      @resource_class ||= resource_name.classify.constantize
    end

    # The singular name for the resource class based on the controller
    # @return [String]
    def resource_name
      @resource_name ||= self.controller_name.singularize
    end

    # Only allow a trusted parameter "white list" through.
    # If a single resource is loaded for #create or #update,
    # then the controller for the resource must implement
    # the method "#{resource_name}_params" to limit permitted
    # parameters for the individual model.
    def resource_params
      @resource_params ||= self.send("#{resource_name}_params")
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_resource(resource = nil)
      resource ||= resource_class.find(params[:id])
      instance_variable_set("@#{resource_name}", resource)
    end

    def no_document_error
      render json: { status: :not_ok, msg: 'document not found' }, status: :not_found
    end

    def invalid_find
      render json: { status: :not_ok, msg: 'check you request params' }, status: :not_found
    end


    def json_request?
      request.format.json?
    end

  end
end