require_relative '../../../../lib/service_object/set_verify_code'

module Api
  class Doc::UsersController < Api::Doc::BaseController
    skip_before_action :doc_token_valid, except:  [  :update_password, :update_avatar, :prescription_list, :patient_list, :upload ]

    def sign_up#用户注册

      pu = Doctor.where(mobile: user_params[:mobile]).first
      raise(Errors::UserAlreadyExisted, "user already existed") if pu.present?
      raise(Errors::PasswordRequired, 'password required') if user_params[:password].blank?
      pu = Doctor.create(user_params).tap do |u|
        u.ensure_private_token!
      end
      SetVerifyCode.used( user_params[:mobile], 'doctor' )
      render json: { status: :ok, msg: 'create successfully', token: pu.private_token }, status: :ok
    rescue Errors::UserAlreadyExisted, Errors::PasswordRequired => e
      render json: { status: :not_ok, msg: e.message }
    end

    def login
      u = Doctor.where(mobile: user_params[:mobile]).last
      raise(Errors::DoctorNotFound, 'patient not found' ) if u.blank?
      if u.authenticate(user_params[:password])
        render json: { status: :ok, msg: 'login successfully', user: u.to_hash }
      else
        render json: { status: :not_ok, msg: 'user password not match' }
      end
    end


    def update_avatar
      raise "avatar is empty" if update_avatar_params[:avatar].blank?
      @doc.avatar = update_avatar_params[:avatar]
      @doc.save
      render json:  { status: :ok, msg: 'update avatar ok', data: { avatar_url: @doc.avatar_url } }
    end

    def update_password
      raise "password is empty" if update_password_params[:password].blank?
      if update_password_params[:origin_password].present?
        raise "origin password not correct"  unless @doc.authenticate(update_password_params[:origin_password])
      end
      @doc.password = update_password_params[:password]
      @doc.save
      render json:  { status: :ok, msg: 'update password ok' }
    end

    def prescription_list
      @prescriptions = Prescription.where(:prescription_doctor_id.in => Array( @doc.prescription_doctor )).page(params[:page] || 1 )
      # product_ids = Order.where( :prescription_id.in => Array(@prescriptions) ).flat_map { |e| e.line_items.pluck(:product_id) }
      @prescriptions.each_with_object( @line_items = {} ) do |pres, hash|
        hash[pres.id.to_s] = pres.order.try(:line_items)
      end
      render 'api/doc/prescriptions/index'
    end
    
    def upload
      raise "id_image file cannt be blank" if params[:id_image].blank?
      raise "doc_license file cannt be blank" if params[:doc_license].blank?
      @doc.id_image = params[:id_image]
      @doc.doc_license = params[:doc_license]
      @doc.save
      render json: { status: :ok, msg: 'upload images ok' }
    end

    def patient_list
      @patients = Patient.where(:id.in => Array( @doc.my_patients)).page(params[:page] || 1)
      @patients.each_with_object(@prescription_hash = {}) do |patient, hash|
        hash[patient.id.to_s] = @doc.prescriptions_for(patient) # 此医生为该病人开的处方

        #TODO: 这个方法耦合度比较高， 需要重构
        hash[patient.id.to_s].each_with_object( @line_items = {} ) do |pres, z| # 每个处方所对应的 line_items
          z[pres.id.to_s] = pres.order.try(:line_items)
        end

      end
    end

    private
    def user_params
      params.permit(:sin, :mobile, :email, :password, :username, :avatar, :sin_image, :detail_address)
    end

    def update_avatar_params
      params.permit(:avatar)
    end

    def update_password_params
      params.permit(:password, :origin_password)
    end
  end
end