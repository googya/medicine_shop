require_relative '../../../lib/service_object/check_message'
require_relative '../../../lib/service_object/set_verify_code'
require_relative '../../../lib/service_object/apply_invitation'
module Api
  class UsersController < Api::BaseController
    # skip_before_action :token_valid, except:  [ :coupons, :check_unread, :count_info, :update_password, :update_avatar, :my_prescription_doctors, :upload ]
    skip_before_action  :token_valid, only: [ :sign_up, :login, :verify_invitation ]
    def sign_up#用户注册
      #TODO 暂时只是 Patient 注册， 后续会有其他类型的用户注册
      pu = Patient.where(mobile: user_params[:mobile]).first
      raise(Errors::UserAlreadyExisted, "user already existed") if pu.present?
      raise(Errors::PasswordRequired, 'password required') if user_params[:password].blank?
      pu = Patient.create( user_params.except(:invitation_code) ).tap do |u|
        u.ensure_private_token!
      end
      SetVerifyCode.used( user_params[:mobile] )
      deal_invitation(pu) if user_params[:invitation_code]

      render json: { status: :ok, msg: 'create successfully', token: pu.private_token }, status: :ok
    rescue Errors::UserAlreadyExisted, Errors::PasswordRequired => e
      render json: { status: :not_ok, msg: e.message }
    rescue Errors::InvalidInvitationCode => e
      render json: { status: :ok, msg: '用户创建成功， 但邀请码不正确', token: pu.private_token }
    end

    def login
      u = Patient.where(mobile: user_params[:mobile]).last
      raise(Errors::PatientNotFound, 'patient not found' ) if u.blank?
      if u.authenticate(user_params[:password])
        render json: { status: :ok, msg: 'login successfully', user: u.to_hash }
      else
        render json: { status: :not_ok, msg: 'user password not match' }
      end
    end

    def check_unread
      cm = CheckMessage.new(@patient)
      info = { orders: cm.has_unread_new_orders?, activities: cm.has_unread_new_activities?  }
      render json: { status: :ok, msg: 'ok', read_info: info }
    end

    def count_info
      info = { shop_cart: Array(@patient.try(:shop_cart).try(:shop_cart_items)).count,  prescription: @patient.prescriptions.count, order: @patient.orders.valid.count  }
      info.merge!( coupons: @patient.coupons.count )
      render json: { status: :ok, msg: 'count info', count_info: info }
    end

    def update_avatar
      raise "avatar is empty" if update_avatar_params[:avatar].blank?
      @patient.avatar = update_avatar_params[:avatar]
      @patient.save
      render json:  { status: :ok, msg: 'update avatar ok', data: { avatar_url: @patient.avatar_url } }
    end
    
    def update_password
      raise "password is empty" if update_password_params[:password].blank?
      if update_password_params[:origin_password].present?
        raise "origin password not correct"  unless @patient.authenticate(update_password_params[:origin_password])
      end
      @patient.password = update_password_params[:password]
      @patient.save
      render json:  { status: :ok, msg: 'update password ok' }
    end

    def my_prescription_doctors
      @pds = @patient.prescription_doctors.page(params[:page] || 1)
    end

    def coupons
      patient = Patient.find(params[:id])
      @coupons = patient.coupons
    end
    
    def upload
      raise "id_image file cannt be blank" if params[:id_image].blank?
      raise "hic_image file cannt be blank" if params[:hic_image].blank?
      @patient.id_image  = params[:id_image]
      @patient.hic_image = params[:hic_image]
      @patient.hic_num   = params[:hic_num] if params[:hic_num]
      @patient.save
      render json: { status: :ok, msg: 'upload images ok' }
    end

    def verify_invitation
      invitation_code = params[:invitation_code]
      raise 'invitation_code is blank' if invitation_code.blank?
      t = ApplyInvitation.new(invitation_code).verify
      render json: { status: :ok, msg: 'verify', result: t.present? }
    end

    private
    def user_params
      return params.permit(:token) if params[:user].blank?
      params.require(:user).permit(:invitation_code, :sin, :mobile, :email, :password, :username, :avatar, :sin_image, :detail_address, :format)
    end

    def update_avatar_params
      params.permit(:avatar)
    end

    def update_password_params
      params.require(:user).permit(:password, :origin_password)
    end

    def deal_invitation pu
      invitation_code = params[:user].fetch(:invitation_code) { nil }
      # ApplyInvitation.new(invitation_code, pu).create_invitation if invitation_code.present? # 处理邀请码
      if(!invitation_code.nil?)
        deal_invitation_flag = ExchangeCode.check_invite_code(invitation_code, pu.id)
        if !deal_invitation_flag
          puts "============"
          puts "============"
          puts "App端"
          puts "============"
          puts "============"
          raise(Errors::InvalidInvitationCode, '该兑换码已失效')
        end
      end
    end
  end
end