module Api
  class ActivitiesController < ApplicationController
    def index
      @activities = Activity.valid.order(created_at: :desc)
      read_activities

    end

    protected

    def load_user
      if params[:token].present?
        token_valid
      end
    end

    def read_activities
      load_user
      if @patient.present?
        payload = { action: 'new_activity', user_id: @patient.id }
        ActiveSupport::Notifications.instrument 'user.has_read_messages', payload
      end
    end
  end
end