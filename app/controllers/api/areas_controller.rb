#encoding: utf-8

class Api::AreasController < Api::BaseController
  skip_before_action :token_valid, only: [:get_cities, :active_cities ]
  def index
    provinces = Area.includes(:children).provinces
    data = provinces.each_with_object({}) do |p, h|
      h[p.name] = p.children.map(&:to_hash)
    end

    render json: { status: :ok, msg: 'list ok', area: data }
  end

  def get_cities
    province = Area.where(id: params[:province_id]).first

    data = province ? province.children.collect{|ele| [ele.name, ele.id.to_s]} : []

    render json: {status: :ok, cities: data}
  end

  def active_cities
    data = Area.where( :name.in => %w[上海 南京 杭州], :level => 3 )
    render json: { status: :ok, cities: data.map(&:to_hash) }
  end

end
