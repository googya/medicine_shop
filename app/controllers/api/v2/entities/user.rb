module API
  module V2
    module Entities
      class User < Grape::Entity
        expose :username, documentation: { type: "string", desc: "用户的名字" }
        expose :mobile,  documentation: { type: "string", desc: "用户的手机号" }
        expose(:id)  {  |user|  user.id.to_s  }
      end
    end
  end
end