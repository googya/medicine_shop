module API
  module V2
    module Defaults
      extend ActiveSupport::Concern

      included do
        prefix "api"
        version "v2", using: :path
        default_format :json
        format :json

        helpers do
          def permitted_params
            @permitted_params ||= declared(params, include_missing: false)
          end

          def logger
            Rails.logger
          end
        end

        rescue_from Mongoid::Errors::DocumentNotFound do |e|
          error_response(message: e.message, status: 404)
        end

      end
    end
  end
end