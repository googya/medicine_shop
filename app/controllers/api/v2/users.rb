module API
  module V2
    class Users < Grape::API
      include API::V2::Defaults

      resource :users do
        desc "Return all users"
        get "", root: :users do
          users = User.all
          present users, with: API::V2::Entities::User
        end

        desc "Return a user"
        params do
          requires :id, type: String, desc: "ID of the user"
        end
        get ":id", root: "user" do
          present User.where(id: permitted_params[:id]).first!, with: API::V2::Entities::User
        end
      end
    end
  end
end