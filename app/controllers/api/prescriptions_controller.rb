#encoding: utf-8
require_relative '../../../lib/service_object/tidy_shop_cart'
module Api
  class PrescriptionsController < Api::BaseController
    # my prescriptions
    def index
      @prescriptions = @patient.prescriptions.order_by(updated_at: :desc)
      payload = {  accountable: @patient }
      ActiveSupport::Notifications.instrument 'user.home_user_message_read', payload

    end

    def create
      raise(Errors::LackImageError, "image required") if prescription_params[:image].blank?
      # raise(Errors::DoctorNotFound, "no such a doctor") if Doctor.where(:id => prescription_params[:doctor_id]).blank?
      #TODO 对医生和病人都需要验证
      t = Prescription.new(prescription_params.slice( :doctor_id, :note, :user_agent, :area_id ))
      t.patient = @patient
      t.image = prescription_params[:image]
      if prescription_params[:items].present?
        t.create_items( prescription_params[:items] )
      end

      # 清空购物车内容
      # 2015-5-27 不需要清空购物车, 仅作清理
      # @patient.shop_cart.try(:delete) #
      tidy_cart( @patient.shop_cart, product_ids_uploaded_with_items )
      
      t.save!
      render json: { status: :ok, msg: 'prescription submitted successfully' }
    rescue  Errors::LackImageError, Errors::DoctorNotFound => e
      render json: { status: :not_ok, msg: e.message }
    rescue => e
      render json: { status: :not_ok, msg: e.message }
    end

    # 这个接口承担太多的功能， 需要重构
    # 
    def create_pres_with_order_items
      t = Prescription.new(prescription_params.slice(:doctor_id, :note, :user_agent, :area_id))
      t.patient = @patient
      t.image = prescription_params[:image]
      if prescription_params[:items].present?
        t.create_items( prescription_params[:items] )
      end

      order_id = prescription_order_params[:order_id]
      if order_id.present?
        order = Order.find(order_id)
        order.prescription = t
        update_old_order(order, prescription_order_params)
      end

      t.save

      render json: { status: :ok, msg: 'update ok' }
    end


    def create_pres_with_order_items_for_android
      t = Prescription.new( for_android_prescription_params.slice(:doctor_id, :note, :user_agent, :area_id))
      t.patient = @patient

      t.image = for_android_prescription_params[:image]
      if for_android_prescription_params[:items].present?
        t.create_items( JSON(for_android_prescription_params[:items]) )
      end

      order_id = for_android_prescription_params[:order_id]
      if order_id.present?
        order = Order.find(order_id)
        order.prescription = t
        update_old_order_for_android(order, for_android_prescription_params)
      end

      t.save

      render json: { status: :ok, msg: 'update ok' }
    end


    private
    def prescription_params
      params.permit(:area_id, :image, :doctor_id, :token, :user_agent,  :note, :format, :patient_id, items: [ :product_id, :price, :quantity ] )
    end

    def prescription_order_params
      params.permit(:area_id, :image, :doctor_id, :user_agent, :token, :note, :format, :patient_id, :payment_method, :address_name, :shipment_method, :address_id,
                     :order_id, :time,  items: [ :product_id, :price, :quantity ] )
    end

    def update_old_order(order, opts)
      return if opts[:items].blank?
      order.line_items.delete_all
      order.payment_method = opts[ :payment_method ]
      # 删除后 新建
      opts[:items].each do |item|
        order.line_items << LineItem.new(quantity: item[:quantity], product_id: item[:product_id])
      end
      order.shipment_method = opts[:shipment_method]
      if opts[:shipment_method] == '药店取药'
        order.temp_shipment_address = opts[:address_name]
      elsif opts[:address_id].present?
        order.ship_address_id = opts[:address_id]
        order.ship_time = opts[:time]
      end
      order.save
    end

    def for_android_prescription_params
      params.permit( :image, :doctor_id, :user_agent, :token, :note, :format, :patient_id, :address_name, :shipment_method, :address_id,
                     :order_id, :time,  :items )
    end

    
    def update_old_order_for_android(order, opts)
      return if opts[:items].blank?
      order.line_items.delete_all
      # 删除后 新建
      JSON(opts[:items]).each do |_item|
        item = _item.with_indifferent_access
        order.line_items << LineItem.new(quantity: item[:quantity], product_id: item[:product_id])
      end

      order.shipment_method = opts[:shipment_method]
      if opts[:shipment_method] == '药店取药'
        order.temp_shipment_address = opts[:address_name]
      elsif opts[:address_id].present?
        order.ship_address_id = opts[:address_id]
        order.ship_time = opts[:time]
      end
      order.save
    end

    def tidy_cart(cart, product_ids = [])
      TidyShopCart.new(cart, product_ids).tidy
    end

    def product_ids_uploaded_with_items
      Array(prescription_params[:items]).map { |item| item['product_id'] }
    end

  end
end