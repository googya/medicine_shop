# encoding: utf-8

class Api::VerifyCodesController < Api::BaseController
  skip_before_action :token_valid
  before_action :set_use_for


  def create
    code = VerifyCode.find_or_create_by( mobile: params[:mobile].strip,  mode: VerifyCode::REG, use_for: set_use_for )
    render json: { status: :not_ok, msg: "该号码已被用过" } and return if code.status == VerifyCode::USED
    code.send_code

    render json: { status: :ok, msg: 'ok', code: code.code}
  end

  def verify
    _code = VerifyCode.where(mobile: params[:mobile], code: params[:verify_code], use_for: set_use_for).first
    if _code && _code.valid_code?
      render json: { status: :ok, msg: 'ok' }
    else
      render json: { status: :not_ok, msg: 'mobile and verify_code not match' }
    end
  end

  private

  def set_use_for
    @use_for = ( params[:use_for].presence || 'patient' )
  end
end
