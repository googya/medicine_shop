# encoding: utf-8

class Api::ExchangeCodesController < Api::BaseController
  def verify
    verify_flag = ExchangeCode.check_invite_code(exchange_code_params[:code], exchange_code_params[:patient_id])
    if verify_flag
      # Todo
      # generate coupon

      render json: { status: :ok, msg: 'ok' }
    else
      render json: { status: :not_ok, msg: '该兑换码已失效' }
    end
  end

  private
  def exchange_code_params
    params.premit(:code, :patient_id)
  end
end
