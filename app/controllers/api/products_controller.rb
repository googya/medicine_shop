# coding: utf-8
require 'service_object/product_in_orders'

module Api
  class ProductsController < Api::BaseController
    skip_before_action :token_valid
    before_action 'token_valid', only: [ :common, :price_list ]
    def index
      @products = Product.all.valid.page(params[:page] || 1)
    end

    def common
      p_ids = ServiceObject::ProductInOrders.new(@patient).common_product_ids
      @products = Product.where( :id.in => p_ids ).valid
      render 'api/products/index'
    end

    def price_list
      # 列出该商品的全部价格
      prices = Price.where(product_id: params[:id])
      render json: { status: :ok, msg: 'all prices of this product', prices: prices.map(&:to_hash) }
    end

    def show
      @product = Product.find(params[:id])
    end

  end
end
