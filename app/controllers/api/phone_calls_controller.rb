class Api::PhoneCallsController < Api::BaseController

  skip_before_action :token_valid

  def create
    pc = PhoneCall.new(phone_call_params)
    pc.user = load_user if load_user.present?
    pc.save

    render json: { status: :ok, msg: 'phone call created ok' }
  end

  private

  def phone_call_params
    params.permit(:name, :mobile)
  end

  def load_user
    return if params[:token].blank?
    User.find_by(private_token: params[:token])
  end

end