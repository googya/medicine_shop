#encoding: utf-8
require_relative '../../../lib/service_object/check_message'
class Api::MessagesController < Api::BaseController

  def latest
    load_event
    if load_event.blank?
      render json: { status: :ok, msg: 'ok', message: nil  }
    else
      render json: { status: :ok, msg: 'ok', message: { title: message_content, id: String(load_target.try(:id)), type: load_event.class.model_name.element, status:  show_status  }  }
    end
  end

  private
  def load_event
    @event ||= CheckMessage.new( @patient ).latest
  end

  def message_content
    load_event.message
  end

  def load_target
    @target ||= load_event.try( load_event.class.model_name.element )
  end

  def show_status
    load_target.is_a?(Prescription) ? load_target.status : load_target.state
  end

end
