# coding utf-8
require_relative '../../../app/models/errors/errors'
require_relative '../../../lib/service_object/gen_order'
require_relative '../../../lib/service_object/coupon/apply_coupon'
require_relative '../../../lib/service_object/coupon/validate'

module Api
  class OrdersController < Api::BaseController

    def index
      patient = Patient.where(private_token: params[:token]).first
      raise(Errors::PatientNotFound, 'user not found ') if patient.blank?
      @orders = patient.orders.valid.order_by( updated_at: :desc )

      payload = { action: 'new_order', user_id: patient.id }
      ActiveSupport::Notifications.instrument 'user.has_read_messages', payload
      ActiveSupport::Notifications.instrument 'user.home_user_message_read', {  accountable: patient }

    rescue Errors::PatientNotFound => e
      render json: { status: :not_ok, msg: e.message }
    rescue => e
      render json: { status: :not_ok, msg: e.message }
    end

    def show
      @order = Order.find(params[:id])
      # render json: @order.to_builder
    end


    def update # update order
      order = Order.find(order_params[:id])
      order.modify_line_items(order_params[:line_items])

      render json: order.to_builder
    # rescue Mongoid::Errors::DocumentNotFound => e
    #   render json: { status: :not_ok, msg: e.message }
    end

    def confirm_order
      check_prescription_status

      OrderBussiness.comfirm_order(
        order: @order,
        attributes: attributes,
        is_taken_by_customer: ['药店取药'].include?(confirm_order_params[:shipment_method])
      )

      apply_coupon( @order ) if confirm_order_params[:coupon_id].present?
      payload = { order: @order, accountable: @patient }
      ActiveSupport::Notifications.instrument 'order.be_confirmed', payload
      render json: { status: :ok, msg: 'order confirmed' }

    end

    def create
      items = order_with_prescription_params[:line_items]
      img =  order_with_prescription_params[:prescription_img]
      order = GenOrder.new( @patient, img, items ).generate

      tidy_cart( @patient.shop_cart, items.map { |e|  e['product_id'] } )

      render json: order.to_builder
    end

    def cancel_order
      order = Order.find( params[:id] )
      order.cancel; order.save

      payload = { order: order, accountable: @patient  }
      ActiveSupport::Notifications.instrument 'order.be_canceled', payload
      render json: { status: :ok, msg: 'order canceled' }

    end

    def verify_coupon
      raise 'no total' if params[:total].blank?
      load_coupon

      result = Coupon::Validate.new( @coupon, params[:total].to_f, @patient ).validate
      render json: { status: :ok, result: result }
    end

    private

    def order_params
      params.permit(:token, :id, line_items: [:price, :quantity, :product_id])
    end

    def order_with_prescription_params
      params.permit(:token, :id, :prescription_img, line_items: [:price, :quantity, :product_id])
    end

    def confirm_order_params
      params.permit(:coupon_id, :token, :_from, :id, :user_agent, :ship_time, :address_id, :shipment_method, :address_name, :payment_method, line_items: [:price, :quantity, :product_id] )
    end

    def check_prescription_status
      @order = Order.find(confirm_order_params[:id])
      if @order.prescription.status != 'approved'
        raise(Errors::PrescriptionNotApproved, 'prescription not approved')
        #TODO
        # 对于全部是非处方药的, 无须验证
      end
    end

    def tidy_cart(cart, product_ids = [])
      TidyShopCart.new(cart, product_ids).tidy
    end

    def apply_coupon(_order)
      coupon = Coupon.where(id: confirm_order_params[:coupon_id]).first
      ::Coupon::ApplyCoupon.new(coupon, _order).apply
    end

    def load_coupon
      raise 'no coupon_id' if params[:coupon_id].blank?
      @coupon = Coupon.find( params[:coupon_id] )
    end

  end
end
