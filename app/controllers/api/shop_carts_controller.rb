# encoding: utf-8

require_relative '../../../lib/service_object/gen_order_without_prescription'
require_relative '../../../lib/service_object/tidy_shop_cart'

module Api
  class ShopCartsController < Api::BaseController

    def create
      ShopCart.where(patient_id: @patient.id).delete_all
      cart = ShopCart.new_with_items(shop_cart_params[:items])
      @patient.shop_cart = cart
      cart.save
      render json: {status: :ok, msg: 'ok'}
    end

    def show
      load_shop_cart
      tidy_cart(@sc)
      render json: { status: :ok, msg: 'ok', shop_cart: @sc.to_builder }
    end

    def destroy
      ShopCart.where(patient_id: @patient.id).delete_all
      render json: { status: :ok, msg: 'delete ok' }
    end


    def inc_item
      load_shop_cart

      item = @sc.inc_item(shop_cart_item_params[:product_id])
      render json: { status: :ok, msg: 'inc item ok', data: item.to_builder }
    end


    def dec_item
      load_shop_cart
      item = @sc.dec_item(shop_cart_item_params[:product_id])
      render json: { status: :ok, msg: 'dec item ok', data: item.to_builder }

    end

    # 购物车结算
    # 1 无处方药时， 直接生成订单，
    # 2 有处方药时， 需先生成处方（跟处方购药逻辑一致）
    def check
      prod_ids = shop_cart_params[:items].each_with_object([]) { |item, arry| arry << item[:product_id] }
      # raise("contain rx drugs") unless Product.all_otc?(prod_ids)

      # 无处方药时， 直接生成订单
      order = GenOrderWithoutPrescription.new(@patient, shop_cart_params[:items]).generate
      order.ship_address_id = shop_cart_params[:address_id]
      order.save!

      tidy_cart(load_shop_cart, prod_ids)

      render json: { status: :ok, msg: 'order created', order_id: order.id.to_s }
    end

    # TODO  清除购物车中部分物品

    private

    def shop_cart_params
      params.permit(:code, :address_id, items: [ :price, :quantity, :product_id ])
    end

    def shop_cart_item_params
      params.permit(:product_id)
    end

    def load_product
      prod = Product.find( shop_cart_item_params[:product_id] )
    end

    def load_shop_cart
      @sc ||= (@patient.shop_cart || @patient.create_shop_cart)
    end

    def tidy_cart(cart, product_ids = [])
      cart = TidyShopCart.new(cart, product_ids).tidy
    end

  end
end
