require_relative '../../../lib/service_object/locator'
module Api
  class DrugStoresController < Api::BaseController
    skip_before_action :token_valid
    def index
      page = params[:page] || 1
      @drug_stores = DrugStore.all.page(page).per(10)
    end

    def search
      raise 'no longitude or latitude' if params[:longitude].blank? || params[:latitude].blank?
      addresses = Locator.drug_stores( params[:longitude], params[:latitude] )

      render json: { status: :ok, datas: addresses }
    end
  end
end
