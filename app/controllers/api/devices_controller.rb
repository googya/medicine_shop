class Api::DevicesController < Api::BaseController

  def register
    fail("device is empty") if device_params[:device_id].blank?
    unbound_others
    de = Device.find_or_initialize_by(devicable: @patient)
    if de.value != device_params[:device_id]
      de.value = device_params[:device_id]
      de.save!
    end
    
    render json: { status: :ok, msg: 'registered ok' }
  end


  private

  def device_params
    params.permit(:device_id, :token)
  end

  def unbound_others
    Device.where(value: device_params[:device_id], :devicable.ne => @patient).update_all(value: nil)
  end
end
