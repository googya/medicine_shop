 # encoding: utf-8

class Api::AddressesController < Api::BaseController
  def index
    addresses = @patient.addresses
    render json: { status: :ok, msg: 'list ok', addresses: addresses.map(&:to_hash) }
  end

  def create
    address = @patient.addresses.create(address_params)
    address.default = true          if @patient.default_shipping_address.nil?
    address.save
    render json:  { status: :ok, msg: 'create address ok', ad: address.to_ary }
  end


  def update
    address = @patient.addresses.where(id: params[:id]).first
    address.update_attributes(address_params)
    address.default = true          if @patient.default_shipping_address.nil?
    address.save
    render json:  { status: :ok, msg: 'update address ok' }
  end

  def set_default
    address = @patient.addresses.where(id: params[:id]).first
    raise("address not found") if address.blank?
    if address.present?
      @patient.set_default_address(address)
    end

    render json: { status: :ok, msg: 'set default address ok' }

  end

  def default
    address = @patient.default_shipping_address
    if address.blank?
      render json: { status: :not_ok, msg: 'no default address' }
    else
      render json: { status: :ok, msg: 'default address', address: address.try(:to_hash) }
    end
  end

  private

  def address_params
    params.permit(:state, :city, :address1, :zip_code, :name, :phone)
  end
end
