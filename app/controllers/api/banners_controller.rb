class Api::BannersController < Api::BaseController
  skip_before_action :token_valid
  def index
    banners = Banner.valid
    render json: { status: :ok, msg: 'show banners ok', data: {  banners: banners.map(&:to_hash) } }
  end

end
