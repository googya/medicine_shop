require 'forwardable'
require_relative '../models/errors/errors'
require_relative '../../lib/weixin/weixin'
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  extend Forwardable
  def_delegator ActiveSupport::Notifications, :instrument

  before_action :set_channel


  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token, if: :json_request?

  before_filter do
    resource = controller_name.singularize.to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)

    if devise_controller?
      devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:mobile, :password, :remember_me) }
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name,  :mobile, :password, :password_confirmation, :avatar) }
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :mobile, :password, :password_confirmation, :avatar) }
    end

    # if browser.mobile?
    #   request.variant = :tablet
    # end

  end

  protected

  def json_request?
    request.format.json?
  end

  private

  def token_valid
    @patient = Patient.where( private_token: String( params[:token].to_s ) ).first
    raise(Errors::TokenInvalid, 'token invalid') if @patient.blank?
    true
  rescue => e
    render json: { status: :not_ok, msg: e.message }
  end


  def doc_token_valid
    @doc = Doctor.where( private_token: String( params[:token].to_s ) ).first
    raise(Errors::TokenInvalid, 'token invalid') if @doc.blank?
    true
  rescue => e
    render json: { status: :not_ok, msg: e.message }
  end

  def set_channel
    @channel ||= Channel.first
  end

  def gon_info
    timestamp = Time.now.to_i
    url = request.url
    noncestr = 'ruby'
    jsapi_ticket = ::Weixin.get_ticket
    signature = ::Weixin.get_sign(jsapi_ticket, noncestr, timestamp, url)

    gon.info = { timestamp: timestamp, signature: signature, noncestr: noncestr, app_id:  WECHAT_CONFIG['appid'], url: url }
  end

end
