# encoding: utf-8

require_relative '../../lib/service_object/gen_ticket'
require_relative '../../lib/weixin/weixin'

class WechatsController < ApplicationController
  wechat_responder

  on :event, with: /SCAN|subscribe/ do |request|
    message_hash = request.message_hash

    ticket       = message_hash[:Ticket]
    openid       = message_hash[:FromUserName]
    action_name  = message_hash[:Event].downcase

    w_user = Patient.where(open_id: openid).first

    request.reply.news(0...1) do |article, i|
      article.item title: "", description:"绑定您的手机号，即可拍处方领药。", pic_url: "#{APP_CONFIG['host']}/mobile/index_reg.jpg", url: Weixin.generate_oauth_path("#{APP_CONFIG['host']}/mobiles/users/new")
    end unless w_user
  end

  # 数据查看
  on :text do |request, content|
    message_hash = request.message_hash

    Rails.logger.info '%' * 100
    Rails.logger.info message_hash
    Rails.logger.info '&' * 100

    case message_hash['Content']
    when /^1[3|4|5|8][0-9]\d{4,8}$/ # 验证手机号
      w_user = Patient.where(open_id: message_hash[:FromUserName]).first
      if w_user && w_user.mobile == message_hash['Content']

      end
    when 'smyz'
      request.reply.text "点击验证 #{Weixin.generate_oauth_path("#{APP_CONFIG['host']}/mobiles/users/valid")}"
    end
  end

end
