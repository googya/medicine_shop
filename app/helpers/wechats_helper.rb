require_relative '../../lib/weixin/weixin'

module WechatsHelper

  def gen_auth_url(path)
    Weixin.generate_oauth_path("#{APP_CONFIG['host']}/#{path}")
  end
end
