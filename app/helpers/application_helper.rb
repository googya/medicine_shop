module ApplicationHelper

  def flash_class(type)
    case type.to_sym
    when :notice
      'alert-success'
    when :alert
      'alert-danger'
    end
  end

end
