module Admin::V2::BaseHelper
  def nav_class_name(sub_name, item_name)
    return 'active' if sub_hash[sub_name] && sub_hash[sub_name].include?(params[:controller].split('/').last) && sub_name == item_name
    return 'active' if params[:controller] == "admin/v2/#{item_name}"
    return 'active' if params[:controller] == "admin/v2/weixin/#{item_name}"
    return ''
  end

  def sub_hash
    {
      'sub_prescriptions' => %w(prescriptions prescription_doctors),
      'sub_dashboards'    => %w(dashboards),
      'sub_medicines'     => %w(medicines hospitals drug_stores doctors departments users admin_users coupons),
      'sub_orders'        => %w(orders),
      'sub_APP'           => %w(activities banners products),
      'sub_weixin'        => %w(weixin/products)
    }
  end


end
