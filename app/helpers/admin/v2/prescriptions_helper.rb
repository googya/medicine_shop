require_relative '../../../../lib/service_object/get_price'
module Admin::V2::PrescriptionsHelper
  def pres_state_to_chinese(state)
    hash = Prescription::STATUS.map(&:to_s).zip(['待审核', '通过', '拒绝']).to_h
    hash.fetch(String(state)) { '未知' }
  end

  def price_in_area(product, area)
    GetPrice.price_at_area(product, area)
  end
end
