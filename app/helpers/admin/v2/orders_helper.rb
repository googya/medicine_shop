module Admin::V2::OrdersHelper
  def state_to_chinese(state)
    hash = Order::STATES.keys.map(&:to_s).zip(['未确认', '待完成', '已完成', '已取消', '待审核']).to_h
    hash.fetch(String(state)) { '未知' }
  end
end
