module Api::PrescriptionsHelper
  def chinese_status(status)
    hash = Hash[ Prescription::STATUS.zip(['药剂师审方', '审方通过', '审方不通过']) ].with_indifferent_access
    hash.fetch(status) { '' }
  end
end
