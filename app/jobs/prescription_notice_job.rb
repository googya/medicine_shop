require_relative '../../lib/service_object/jpush_notice_message'

class PrescriptionNoticeJob < ActiveJob::Base
  queue_as :default

  def perform(*args)
    event = Logs::Prescription.where(:id => args[0]).first
    register_id = event.accountable.device_value

    if event && register_id

      msg = if event.status == 'declined'
              "您的处方未通过审核， 理由是， #{ event.prescription.try(:declined_reason) }"
            elsif event.status == 'approved'
              '您的处方已经审核通过，请前往"我的订单"确认订单'
            else
              'something went wrong'
            end

      JPushNoticeMessage.send_msg( register_id, msg )
    end

  end
end
