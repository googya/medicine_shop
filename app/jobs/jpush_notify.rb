require_relative '../../lib/service_object/jpush_notice_message'

class JPushNotify < ActiveJob::Base

  queue_as :default

  def perform(*args)
    # event = Logs::Order.where(:id => args[0]).first
    klass, id, message  = args[0..2]
    event = klass.classify.constantize.find_by(id: id)
    register_id = event.accountable.device_value

    if event && register_id
      JPushNoticeMessage.send_msg( register_id, message  )
    end

  end
end


