
require_relative '../../lib/service_object/sms_sender'

class SmsJob < ActiveJob::Base
  queue_as :default

  def perform(*args)
    msg, numbers = args[0..1]
    SmsSender.send_sms(msg, numbers)
  end
end