require 'order_bussiness/use_cases/confirm_order'
module OrderBussiness
  def self.comfirm_order(order:, attributes:, is_taken_by_customer:)
    OrderBussiness::UserCases::ConfirmOrder.new(
      order: order,
      attributes: attributes,
      is_taken_by_customer: is_taken_by_customer
    ).confirm
  end
end
