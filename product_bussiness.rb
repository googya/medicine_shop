module ProductBussiness
  def self.build_product(attributes:, channel:)
    BuildProduct.new(attributes: attributes, channel: channel).build
  end
end
